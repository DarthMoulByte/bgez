import threading
import sys

from bgez import config

LOCAL = threading.local()
LOCAL.INDENTATION = []
LOCAL.GROUPS = []

def getIndent():
    return ''.join(LOCAL.INDENTATION)

def addIndent(*indents):
    if len(indents) == 0:
        indents = ('  ',)
    for indent in indents:
        LOCAL.INDENTATION.append(indent)

def endIndent(n=1):
    for i in range(n):
        if len(LOCAL.INDENTATION) > 0:
            LOCAL.INDENTATION.pop()

def clearIndents():
    LOCAL.INDENTATION.clear()

def getGroup():
    return '[{}]'.format(']['.join(LOCAL.GROUPS)) if LOCAL.GROUPS else ''

def addGroup(*groups):
    for group in groups:
        LOCAL.GROUPS.append(group)

def endGroup(n=1):
    for i in range(n):
        if len(LOCAL.GROUPS) > 0:
            LOCAL.GROUPS.pop()

def clearGroups():
    LOCAL.GROUPS.clear()

def getDecorations(indent=True, group=True):
    return '{}{}'.format(getGroup(), getIndent())

def w(*args, tag:str='', end:str='\n', fd=sys.stdout):
    """Writes to stdout."""
    if len(args) == 0:
        return fd.write(end)
    if tag:
        tag = '{{{}}}'.format(tag)
    label = '{}{}'.format(getDecorations(), tag)
    if label: label += ' '
    fd.write('{}{}{}'.format(
        label,
        ' '.join(str(arg) for arg in args),
        end
    )); return fd.flush()

def e(*args, **kargs):
    """Writes to stderr."""
    return w(*args, fd=sys.stderr, **kargs)

def d(*args, **kargs):
    """Writes to stderr if debug mode is set."""
    if config.get('BGEz').getboolean('Debug', False):
        return e(*args, **kargs)
