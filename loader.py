import typing as T
import importlib
import pkgutil
import os.path
import sys

from bgez import log

LOADED = {} # type: T.Mapping[str, T.Any]

def ImportModule(*args, **kargs) -> T.Mapping[str, T.Any]:
    log.addGroup('DynImport')
    DynImport(*args, **kargs)
    log.endGroup()
    return LOADED

def DynImport(
    package, # String or actual package
    recursive:bool = True,
    verbose:bool = False
        ) -> T.Mapping[str, T.Any]:
    """Dynamic and recursive importation"""
    loaded = {}
    if isinstance(package, str):
        package = importlib.import_module(package)
    loaded[package.__name__] = package

    if verbose:
        log.d('({})'.format(package.__name__))
        log.addIndent(' .')

    if not hasattr(package, '__file__'):
        raise RuntimeWarning('Module {} might be missing a __ini__.py file...'.format(package.__name__))

    path = os.path.dirname(package.__file__)
    for loader, name, is_pkg in pkgutil.walk_packages((path,)):
        subname = '{}.{}'.format(package.__name__, name)
        if subname in loaded or name.startswith('_'): continue
        if not is_pkg:

            if verbose:
                log.d('Loading > {}...'.format(subname))

            loaded[subname] = importlib.import_module(subname)
        elif recursive: DynImport(subname, recursive=True, verbose=verbose)

    if verbose:
        log.endIndent()

    LOADED.update(loaded)
    return loaded

def ReloadModules(modules:T.Optional[str] = None):
    for name, module in LOADED.items():
        if modules is None:
            importlib.reload(module)
        else:
            for filter in modules:
                if isinstance(name, str) and (name == filter):
                    importlib.reload(module)
