import sys
import bge

from bgez.loader import ImportModule
from bgez import ROOT
from bgez import Component
from bgez import Service
from bgez import config
from bgez import log

def getLogicModule():
    from glob import glob
    from os import path

    module = config.get('Game')['GameLogic']

    if path.isdir(path.join(ROOT, module)):
        return module

    for file in glob(path.join(ROOT, '*.blend')):
        if file[-1].isnumeric(): continue
        dirname = path.splitext(file)[0]
        if path.isdir(dirname):
            module = path.basename(dirname)
            config.get('Game')['GameLogic'] = module
            return module

    return None # Explicit

gamelogic = getLogicModule()
if gamelogic is None:
    log.d('no gamelogic folder found', tag='WARNING')
else:
    ImportModule(gamelogic, verbose=True)
    log.e('[BGEz] Ready.\n')

__all__ = ['init', 'tick']

# At this point everything should be loaded
Component.Initialize()
init = lambda *args: None # Empty entry point
tick = Service.GameManager.tick_dispatch
Service.GameManager.start_dispatch()
