from bgez.framework.managers import Manager
from bgez import Component
from bgez import Service
from bgez import config
from bgez import utils

from bge import logic
from bge import types

__all__ = ['SceneManager']

@Service.Register()
class SceneManager(Manager):
    PARENT = Service.GameManager

    # Default scene wrapper
    FallbackScene = config.get('Fallbacks')['Scene']

    @classmethod
    def mutate(cls, scene):
        if type(scene) is types.KX_Scene:
            scene = Component.GetComponentById(
                cls.FallbackScene
            )(scene)
        return scene

    def getScene(self, *args, **kargs):
        return self.mutate(utils.getScene(*args, **kargs))

    def addScene(self, name, overlay=True):
        return self.mutate(logic.addScene(name, overlay))

    def start(self, scene):
        self.mutate(scene)

    def pre_draw_setup(self, scene):
        return
        print(scene)
