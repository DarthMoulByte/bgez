from bgez.framework.managers import Manager
from bgez.framework.objects import Event
from bgez import Service

import bgez

__all__ = ['InputManager']

@Service.Register()
class InputManager(Manager):
    PARENT = Service.GameManager

    def __init__(self, *args, **kargs):
        self.inputs = set()
        super().__init__(*args, **kargs)

    def RegisterInput(self, input):
        self.inputs.add(input)
        return input

    def pre_draw_setup(self, scene):
        for input in self.inputs:
            if not input.enabled:
                continue

            event = Event()
            for action, mappingSet in input.mappings.items():

                for mapping in mappingSet:
                    mapping.process()

                    if mapping.evaluate():
                        mapping.truthTick = bgez.getTick()
                        event[action] = mapping.getResult()

            # LETS DO THIS
            input.executeCallbacks(event)

            if input.lockedMouse:
                bgez.render.setMousePosition(
                    bgez.render.getWindowWidth() // 2,
                    bgez.render.getWindowHeight() // 2
                )
