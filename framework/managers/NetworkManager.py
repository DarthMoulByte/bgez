from bgez.framework.managers import Manager
from bgez import Service

import selectors
import socket

__all__ = ['NetworkManager']

@Service.Register()
class NetworkManager(Manager):
    PARENT = Service.GameManager

    SELECTOR = selectors.DefaultSelector()

    def start(self, scene):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('127.0.0.1', 2211))
        self.SELECTOR.register(self.socket,
            selectors.EVENT_READ | selectors.EVENT_WRITE, 1024)

    def post_draw(self, scene):
        if self.SELECTOR._readers or self.SELECTOR._writers:
            for key, mask in self.SELECTOR.select():

                socket = key.fileobj
                buffer = key.data

                if mask & selectors.EVENT_READ:
                    print(buffer, socket.recvfrom(buffer))

    def finalize(self, scene):
        self.socket.close()
