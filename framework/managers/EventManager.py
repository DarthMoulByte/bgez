from bgez.framework.managers import Manager
from bgez.framework.bases import Callback
from bgez.framework.bases import Process
from bgez import Service
from bgez import utils

from collections import defaultdict
from collections import deque

__all__ = ['EventManager']

@Service.Register()
class EventManager(Manager):
    PARENT = Service.GameManager

    def __init__(self, *args, **kargs):
        self.events = Service.GlobalEvents
        super().__init__(*args, **kargs)

    # ------------------------------------------------------- #
    # Decorators

    def onStart(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.START)
        return callback

    def onPreDrawSetup(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.PRE_DRAW_SETUP)
        return callback

    def onPreDraw(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.PRE_DRAW)
        return callback

    def onTick(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.TICK)
        return callback

    def onPostDraw(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.POST_DRAW)
        return callback

    def onQuit(self, callback):
        """Decorator"""
        self.events.registerCallback(callback, Process.Event.QUIT)
        return callback

    # ------------------------------------------------------- #
    # Management

    def start(self, scene):
        self.events.addHandler(Process.Event.START)
        self.events.addHandler(Process.Event.TICK)
        self.events.addHandler(Process.Event.PRE_DRAW_SETUP)
        self.events.addHandler(Process.Event.PRE_DRAW)
        self.events.addHandler(Process.Event.POST_DRAW)
        self.events.addHandler(Process.Event.FINALIZE)
        self.events.trigger(Process.Event.START, utils.getTick())

    def tick(self, scene):
        self.events.trigger(
            Process.Event.TICK, utils.getTick())

    def pre_draw_setup(self, scene):
        self.events.trigger(
            Process.Event.PRE_DRAW_SETUP, utils.getTick())

    def pre_draw(self, scene):
        self.events.trigger(
            Process.Event.PRE_DRAW, utils.getTick())

    def post_draw(self, scene):
        self.events.trigger(
            Process.Event.POST_DRAW, utils.getTick())

    def finalize(self, scene):
        self.events.trigger(
            Process.Event.FINALIZE, utils.getTick())
