from bgez.framework.managers import Manager
from bgez import Component
from bgez import Service
from bgez import config
from bgez import utils
from bgez import log

from collections import deque
import bge

__all__ = ['StageManager']

@Service.Register()
class StageManager(Manager):
    PARENT = Service.GameManager

    def MainStage(self, stage):
        """This is a stage class decorator"""
        self.MAIN_STAGE = stage
        return stage

    def addStage(self, stage):
        self.ToAdd.append(stage)

    def removeStage(self, stage):
        self.ToRemove.append(stage)

    def loadStage(self, stage):
        self.LoadedStages.append(stage)
        stage.load()

    def unloadStage(self, stage):
        self.LoadedStages.remove(stage)
        stage.unload()

    def replaceStage(self, old, new):
        if old not in self.LoadedStages:
            raise ValueError('Stage {} is not loaded')
        self.removeStage(old)
        self.addStage(new)

    def loadMainStage(self):
        log.addGroup(self.__class__.__name__)
        if self.MAIN_STAGE is not None:

            if isinstance(self.MAIN_STAGE, str):
                try:
                    stageClass = Component.GetComponentById(self.MAIN_STAGE)
                except KeyError:
                    log.e("requested stage '{}' is not registered.".format(stage_id))

            elif isinstance(self.MAIN_STAGE, type):
                self.addStage(self.MAIN_STAGE())

            else:
                self.addStage(self.MAIN_STAGE)

        log.endGroup()

    # ------------------------------------------------------- #
    # Management

    def __init__(self, *args, **kargs):
        self.ToAdd = deque()
        self.ToRemove = deque()
        self.LoadedStages = []
        self.MAIN_STAGE = None
        super().__init__(*args, **kargs)

    def pre_draw_setup(self, scene):

        while self.ToRemove:
            self.unloadStage(self.ToRemove.popleft())

        while self.ToAdd:
            self.loadStage(self.ToAdd.popleft())

        for stage in self.LoadedStages.copy():
            if scene.name in stage.Scenes:
                stage.pre_draw_setup_dispatch(scene)

    def pre_draw(self, scene):
        for stage in self.LoadedStages.copy():
            if scene.name in stage.Scenes:
                stage.pre_draw_dispatch(scene)

    def tick(self, scene):
        for stage in self.LoadedStages.copy():
            if scene.name in stage.Scenes:
                stage.tick_dispatch(scene)

    def post_draw(self, scene):
        for stage in self.LoadedStages.copy():
            if scene.name in stage.Scenes:
                stage.post_draw_dispatch(scene)

    def finalize(self, scene):
        for stage in self.LoadedStages.copy():
            self.unloadStage(stage)
