from bgez.framework.managers import Manager
from bgez import Component
from bgez import Service
from bgez import config
from bgez import utils

from weakref import WeakSet
from bge import types

__all__ = ['EntityManager']

@Service.Register()
class EntityManager(Manager):
    PARENT = Service.GameManager
    """
    This class offers entity management by providing links model->object.
    Decorate your model class with @EntityManager.link[XXX](...) and
        just watch it work.
    """

    """This property stores the fallback model to apply"""
    FallbackEntities = {
        types.BL_ArmatureObject: config.get('Fallbacks')['ArmatureObject'],
        types.KX_NavMeshObject: config.get('Fallbacks')['NavMeshObject'],
        types.KX_LightObject: config.get('Fallbacks')['LightObject'],
        types.KX_GameObject: config.get('Fallbacks')['GameObject'],
        types.KX_Camera: config.get('Fallbacks')['CameraObject'],
    }

    def __init__(self, *args, **kargs):
        self._links = {}
        super().__init__(*args, **kargs)

    def applyFunction(self, klass, func, *args, **kargs):
        """Can be overriden in order to filter which instance to apply"""
        for instance in klass.GetInstances().copy():
            #if not instance.invalid:
            utils.ftry(func, instance, *args, **kargs)

    class _boundmethod(object):
        """This class is to be used as a method decorator."""
        def __init__(self, manager, method):
            self.method = method # The method to extend
            self.manager = manager # The manager in charge of the instance
            self.owner_id = utils.getQualName(method, 1) # The class qualname
        def __get__(self, object=None, klass=None):
            if object is None and klass is None:
                raise RuntimeError('something went wrong (object=None, klass=None)')
            elif object is None and klass is not None:
                """Here we don't have any instance passed, but we got the class
                (Class.method call)"""
                return lambda *args, **kargs: self.manager.applyFunction(klass, self.method, *args, **kargs)
            else:
                """Here we have the instance
                (Instance.method call)"""
                return lambda *args, **kargs: self.method(object, *args, **kargs)
        def __call__(self, *args, **kargs):
            """When the method is called from outside the object
            (Typically from a registered event)"""
            klass = self.manager.__class__.GetComponentById(self.owner_id)
            return self.manager.applyFunction(klass, self.method, *args, **kargs)

    def boundmethod(self, method):
        """Actual decorator for binding method to its instances"""
        return self._boundmethod(self, method)

    def getObject(self, *args, **kargs):
        """Get an object and link its model"""
        object = utils.getObject(*args, **kargs)
        return self.autoLink(object)[0]

    def spawn(self, target, reference=None, time=0, scene=None, link=True, model=None, children=True, groupMembers=True):
        """spawn an object and link its model"""
        object = utils.addObject(target, reference, time, scene=scene)
        if model is not None:
            if isinstance(model, str):
                object = Component.GetComponentById(model)(object)
            object = model(object)
        elif link:
            object = self.autoLink(object)[0]
        if link:
            self.autoLink(object, children=children, groupMembers=groupMembers)
        return object

    def link(self, *filters):
        """Just give a list of filters (predicates) and you're done.
        E.g:

        @EntityManager.link(
            lambda object: testOnMyObject(object),
            lambda object: anotherTest(object),
        ) # This way both lambda expressions should return true to link any object
        class MyObject(Entity):
            ...
        """
        def decorator(entity):
            """Actual class decorator"""
            checker = lambda object: all(filter(object) for filter in filters)
            self._links[checker] = entity
            return entity

        return decorator

    def linkToObjects(self, *object_names):
        """Use it on a class you want to link to an object"""
        return self.link(lambda object: object.name in object_names)

    def linkToProperties(self, *property_names):
        """Use it on a class you want to link to an object with properties"""
        return self.link(lambda object: any(prop in property_names for prop in object.getPropertyNames()))

    def autoLink(self, *objects, children=True, groupMembers=True):
        """This method converts KX_GameObjects from a list to
            the linked model of your choice. It also checks if an
            object is still valid.
        """
        mutated = []
        for object in objects: # Parse all passed objects

            if isinstance(object, types.KX_GameObject) and object.invalid:
                object = None # This object is now invalid
            elif type(object) in self.FallbackEntities: # Check if they are vanilla
                for filter, model in self._links.items(): # Filters
                    if filter(object): # Does the model apply ?
                        object = model(object)
                        break # Exit filter loop
                if type(object) in self.FallbackEntities:
                    object = Component.GetComponentById(
                        self.FallbackEntities[type(object)]
                    )(object) # At least mutate as a standard Entity

            if object is not None:
                if children and object.children is not None:
                    self.autoLink(*object.children, children=children, groupMembers=groupMembers)
                if groupMembers and object.groupMembers is not None:
                    self.autoLink(*object.groupMembers, children=children, groupMembers=groupMembers)

            mutated.append(object)
        return mutated

    def start(self, scene):
        self.autoLink(*scene.objects)
