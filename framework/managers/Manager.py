from bgez.framework.bases import Process

from bge import logic

__all__ = ['Manager']

class Manager(Process):
    PARENT = None
    """
    It works be managing its own events and some sub managers,
        each dealing with its own events.
    The point is to centralize action execution under the 'tick()' method.
    In order to create another manager, subclass EventManager and decorate
        it with 'RegisterSubManager()'.
        E.g:

            @Manager.RegisterSubManager
            class MyManager(Manager):
                ...

                def manage():
                    # Your management here
                    ...

    This pattern is meant to allow some optimizations processing your events/actions.
    """

    def start_dispatch(self, *args):
        super().start_dispatch(logic.getCurrentScene())

    def tick_dispatch(self, *args):
        super().tick_dispatch(logic.getCurrentScene())

    def pre_draw_setup_dispatch(self, *args):
        super().pre_draw_setup_dispatch(logic.getCurrentScene())

    def pre_draw_dispatch(self, *args):
        super().pre_draw_dispatch(logic.getCurrentScene())

    def post_draw_dispatch(self, *args):
        super().post_draw_dispatch(logic.getCurrentScene())

    def finalize_dispatch(self, *args):
        super().finalize_dispatch(logic.getCurrentScene())

    def __init__(self, *args, **kargs):
        if self.PARENT is not None:
            self.PARENT.register(self)
        super().__init__(*args, **kargs)

    def registerToScene(self, scene):
        if scene.get(self, True):
            scene.pre_draw_setup.append(self.pre_draw_setup_dispatch)
            scene.pre_draw.append(self.pre_draw_dispatch)
            scene.post_draw.append(self.post_draw_dispatch)
            scene[self] = False
            return True
        return False
