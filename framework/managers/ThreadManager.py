from bgez.framework.managers import Manager
from bgez.framework.threading import Thread
from bgez import Service
from bgez import config
from bgez import utils
from bgez import log

import threading
import weakref
import bgez
import gc

__all__ = ['ThreadManager']

@Service.Register()
class ThreadManager(Manager):
    PARENT = Service.GameManager

    def tick(self, scene):
        for thread in Thread.Enumerate():
            if not thread.killed:
                utils.ftry(thread.tick)

    def finalize(self, scene):
        log.addGroup(self.__class__.__name__)

        threads = list(Thread.Enumerate())
        timeout = config.get('Threads').getint('tiemout', 1)

        every = weakref.WeakSet(threading.enumerate())
        if len(every) > 1:
            log.e('{} Threads from builtin.threading... (including MainThread)'.format(len(every)))
            log.e(', '.join(thread.name for thread in every), tag='Names:')

        running = len(threads)
        if running > 0:
            log.e('{} Threads from bgez.threading, killing spree...'.format(len(threads)))

            log.d('Timeout: {}s'.format(timeout))
            for thread in threads:
                thread.kill()

            log.addIndent(' -')

            exited = 0
            for thread in threads:
                thread.join(timeout)
                if not thread.is_alive():
                    log.e('{} exited'.format(thread.getName()))
                    exited += 1
                else:
                    log.e('{} took too long...'.format(thread.getName()))

            log.endIndent()

            log.e('{}/{} ({:.0f}%) killed !'.format(exited, running, 100 * exited / running))

        log.endGroup()

        del threads
        gc.collect()
