from bgez.framework.managers import Manager
from bgez.framework.libraries import Library
from bgez.framework.types import LibLoadStatus
from bgez import Service
from bgez import utils
from bgez import log

import typing as T
import time
import bge

__all__ = ['LibraryManager']

@Service.Register()
class LibraryManager(Manager):
    PARENT = Service.GameManager

    def __init__(self, *args, **kargs):
        self.LOADED = dict()
        self.LOADING = dict()
        super().__init__(*args, **kargs)

    def finishLibLoad(self, status:bge.types.KX_LibLoadStatus):
        self.LOADED[status.libraryName] = self.LOADING.pop(
            status.libraryName, status)

    def libLoad(self, *libraries, unload=False):
        log.addGroup(self.__class__.__name__)
        status = None

        toLoad = []
        for library in libraries:

            # Prepare cleanup name list
            toLoad.append(library.libraryName)

            # Avoid reloading the same library
            if library.libraryName in self.LOADED:
                log.d('[{}] was already loaded'.format(library.libraryName))
                status = self.LOADED[library.libraryName]

            # Loading the library if not already being loaded
            elif library.libraryName not in self.LOADING:

                status = library.load()
                if not status.finished:
                    self.LOADING[library.libraryName] = library
                    status.onFinish = self.finishLibLoad
                else:
                    self.finishLibLoad(library)

        # Cleaning the libs
        if unload:
            for libraryName, library in self.LOADED.copy().items():
                if libraryName not in toLoad:
                    self.unLoad(library)

        log.endGroup()
        return status

    def unLoad(self, library: Library):
        if library.libraryName in self.LOADED:
            library.unload()
            del self.LOADED[library.libraryName]
            return True
        return False

    # def pre_draw_setup(self, scene):
    #     for library in self.LOADING.values():
    #         utils.ftry(library.libstatus.executeCallbacks)
