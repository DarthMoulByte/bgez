from bgez.framework.managers import Manager
from bgez import Service
from bgez import utils
from bgez import log

from weakref import finalize
import bge

__all__ = ['GameManager']

class Canary(object):
    """Canary object to trigger on quit events"""
    pass

class Immortal(object):
    def endObject(self): return print(self.name, 'cannot be ended')
class ImmortalObject(Immortal, bge.types.KX_GameObject): pass
class ImmortalCamera(Immortal, bge.types.KX_Camera): pass

@Service.Register()
class GameManager(Manager):
    """This is the bigboss of all managers
    In a nutshell it only dispatch calls.
    This manager operates at 'Game Level'."""

    def start(self, scene):
        controller = bge.logic.getCurrentController()
        controller.useHighPriority = True
        ImmortalObject(controller.owner)

        fallbackCamera = scene.objects.get('bgez.camera.default', None)
        if fallbackCamera: ImmortalCamera(fallbackCamera)

        # Registering the post/pre draw methods
        scene = Service.SceneManager.getScene()
        self.registerToScene(scene)

        # Registering the Canary for finalizing step
        bge.logic.canary = Canary()
        finalize(bge.logic.canary, self.finalize_dispatch)

    def pre_draw_setup(self, scene):
        log.addGroup(self.__class__.__name__)
        scenes = bge.logic.getSceneList()

        if scene is scenes[0]:
            utils.incrementTick()
        # log.d(scenes, scene, utils.getTick())

        for scn in scenes:
            if self.registerToScene(scn):
                log.d('Scene added: {}'.format(scn.name))

        log.endGroup()
