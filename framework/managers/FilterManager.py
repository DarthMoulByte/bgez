from bgez.framework.managers import Manager
from bgez import Service
from bgez import utils
from bgez import log

import bgez
import traceback

__all__ = ['FilterManager']

@Service.Register()
class FilterManager(Manager):
    PARENT = Service.GameManager

    CREATE = bgez.logic.RAS_2DFILTER_CUSTOMFILTER
    DISABLED = bgez.logic.RAS_2DFILTER_DISABLED
    ENABLED = bgez.logic.RAS_2DFILTER_ENABLED
    DELETE = bgez.logic.RAS_2DFILTER_NOFILTER

    def __init__(self, *args, **kargs):
        self.FilterActuator = None
        self.Filters = {}
        super().__init__(*args, **kargs)

    def TriggerActuator(self, passindex, mode):
        if self.FilterActuator is None: return
        self.FilterActuator.passNumber = passindex
        self.FilterActuator.mode = mode

        try:
            bgez.CONTROLLER.activate(self.FilterActuator)
        except SystemError: pass
        except: traceback.print_exc()

    def ClearFilters(self):
        if self.FilterActuator is None: return
        for passindex in self.Filters:
            self.DeleteFilter(passindex=passindex)

    def DeleteFilter(self, filter=None, passindex=None):
        if self.FilterActuator is None: return
        passindex = passindex or filter.passindex
        self.TriggerActuator(passindex, self.DELETE)

    def RegisterFilter(self, filter):
        if self.FilterActuator is None: return
        passindex = filter.passindex
        if passindex in self.Filters:
            utils.ftry(self.Filters[passindex].quitDispatch)
        self.Filters[passindex] = filter
        self.FilterActuator.shaderText = filter.getShaderSource()
        self.TriggerActuator(passindex, self.CREATE)

    def EnableFilter(self, filter=None, passindex=None):
        if self.FilterActuator is None: return
        passindex = passindex or filter.passindex
        self.TriggerActuator(passindex, self.ENABLED)

    def DisableFilter(self, filter=None, passindex=None):
        if self.FilterActuator is None: return
        passindex = passindex or filter.passindex
        self.TriggerActuator(passindex, self.DISABLED)

    def start(self, scene):
        log.addGroup(self.__class__.__name__)
        for actuator in bgez.CONTROLLER.actuators:
            if isinstance(actuator, bgez.types.SCA_2DFilterActuator):
                self.FilterActuator = actuator
                log.d('2D Filter actuator found, 2D filters should work.')
                break
        if self.FilterActuator is None:
            log.d('2D Filter actuator not found. 2D Filter Shaders will not work.')
        log.endGroup()

    def pre_draw_setup(self, scene):
        if self.FilterActuator is None: return
        for passindex, filter in self.Filters.items():
            for name, value in filter:
                bgez.CONTROLLER.owner[name] = value
