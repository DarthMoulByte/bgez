from bgez import Service

from collections import defaultdict
from collections import deque
import inspect
import types

from .Handle import *

__all__ = ['EventHandler']

def iscoroutine(object):
    return inspect.isgenerator(object) \
        or inspect.iscoroutine(object)

def iscreated(object):
    if inspect.isgenerator(object):
        return inspect.getgeneratorstate(object) is inspect.GEN_CREATED
    elif inspect.iscoroutine(object):
        return inspect.getcoroutinestate(object) is inspect.CORO_CREATED
    return False

@Service.Register('GlobalEvents')
class EventHandler(object):
    '''This object stores listeners and triggers them'''
    __slots__ = ('persist',
        '_listeners', '_callbacks', '_coroutines', '_handlers')

    RECURSION = False
    PERSISTANCE = False

    @staticmethod
    def run(coroutine, *args, next=None):
        try:
            while True:
                yielded = coroutine.send(next)
                next = yielded(coroutine, *args)
                if next is None: return

        except StopIteration:
            return

    def __init__(self, persist=False, *args, **kargs):
        self._coroutines = deque()
        self._callbacks = deque()
        self._listeners = list()
        self._handlers = dict()
        self.persist = persist
        super().__init__(*args, **kargs)

    @property
    def empty(self):
        '''Safe for deletion after .tigger() ?'''
        return not (self.persist
            or self._handlers
            or self._listeners
            or self._callbacks
            or self._coroutines)

    def clean(self, event):
        if self._handlers[event].empty:
            del self._handlers[event]

    def __str__(self):
        handlers = tuple(self._handlers.keys())
        return (
        '<[{}] object @ 0x{:X};\n... listeners: {}; callbacks: {}; coroutines: {}; handlers: {}\n... {}>'.format(
            self.__class__.__name__, id(self),
            len(self._listeners),
            len(self._callbacks),
            len(self._coroutines),
            len(handlers), handlers
        ))

    def __call__(self, *args):
        '''shortcut for .trigger(...)'''
        return self.trigger(*args)

    # ------------------------------------------------------------------------ #
    # Handlers - Sub-events system

    def __getattr__(self, attribute):
        return self._handlers[attribute]

    def addHandler(self, *events, handler_factory=None, persist=PERSISTANCE):
        if not events: return self
        if handler_factory is None:
            handler_factory = self.__class__
        handler = self._handlers.setdefault(events[0], handler_factory(persist))
        return handler.addHandler(*events[1:],
            handler_factory=handler_factory, persist=persist)

    def getHandler(self, *events):
        if not events: return self
        return self._handlers[events[0]].getHandler(*events[1:])

    def removeHandler(self, *events):
        return self.getHandler(*events[:-1]).pop(events[-1])

    # ------------------------------------------------------------------------ #
    # Listeners - Handler as functions

    def registerListener(self, listener, *events, handle=None):
        if handle is None:
            handle = Handle()

        if not events:
            self._listeners.append(listener)
            return handle.register(listener, self._listeners)
        else: # Go deeper
            return self.addHandler(events[0]).registerListener(
                listener, *events[1:], handle=handle)
        return self

    def unregisterListener(self, listener, *events, recursive=RECURSION):
        if not events:
            if listener in self._listeners:
                self._listeners.remove(listener)

            if recursive:
                for event, handler in self._handlers.copy().items():
                    handler.unregisterListener(listener, recursive=recursive)
                    self.clean(event)

        elif events[0] in self._handlers:
            self._handlers[events[0]].unregisterListener(
                listener, *events[1:], recursive=recursive)
            self.clean(events[0])

        return self

    # ------------------------------------------------------------------------ #
    # Callbacks - One shot functions

    def registerCallback(self, callback, *events, handle=None):
        if handle is None:
            handle = Handle()

        if not events:
            self._callbacks.append(callback)
            return handle.register(callback, self._callbacks)
        else: # Go deeper
            return self.addHandler(events[0]).registerCallback(
                callback, *events[1:], handle=handle)

    def unregisterCallback(self, callback, *events, recursive=RECURSION):
        if not events:
            if callback in self._callbacks:
                self._callbacks.remove(callback)

            if recursive:
                for event, handler in self._handlers.copy().items():
                    handler.unregisterCallback(callback, recursive=recursive)
                    self.clean(event)

        elif events[0] in self._handlers:
            self._handlers[events[0]].unregisterCallback(
                callback, *events[1:], recursive=recursive)
            self.clean(events[0])

        return self

    # ------------------------------------------------------------------------ #
    # Coroutines - Partial execution

    def registerCoroutine(self, coroutine, *events, handle=None):
        if handle is None:
            handle = Handle()

        if not events:
            self._coroutines.append(coroutine)
            return handle.register(coroutine, self._coroutines)
        else: # Go deeper
            return self.addHandler(events[0]).registerCoroutine(
                coroutine, *events[1:], handle=handle)

    def unregisterCoroutine(self, coroutine, *events, recursive=RECURSION):
        if not events:
            if coroutine in self.coroutine:
                self._coroutines.remove(coroutine)

            if recursive:
                for event, handler in self._handlers.copy().items():
                    handler.unregisterCoroutine(coroutine, recursive=recursive)
                    self.clean(event)

        elif events[0] in self._handlers:
            self._handlers[events[0]].unregisterCoroutine(
                coroutine, *events[1:], recursive=recursive)
            self.clean(events[0])

        return self

    # ------------------------------------------------------------------------ #
    # Execution

    def executeFunction(self, function, *args):
        returned = function(*args)
        if iscoroutine(returned):
            self._coroutines.append(returned)

    def executeCoroutine(self, coroutine, *args):
        self.run(coroutine, *args, next=(
            None if iscreated(coroutine) else args))

    def execute(self, object, *args):
        if iscoroutine(object):
            return self.executeCoroutine(object, *args)
        elif callable(object):
            return self.executeFunction(object, *args)
        else:
            raise Exception('object passed cannot be executed:  {}'.format(object))

    def trigger(self, *events):
        '''Triggers every callback for (*events)'''
        if self.empty: return # Shorcut

        listeners = self._listeners[:]
        coroutines = self._coroutines
        callbacks = self._callbacks

        for listener in listeners:
            self.executeFunction(listener, *events)

        while callbacks or coroutines:

            while callbacks:
                callback = callbacks.popleft()
                self.executeFunction(callback, *events)

            while coroutines:
                coroutine = coroutines.popleft()
                self.executeCoroutine(coroutine, *events)

        if not events: return
        handler = self._handlers.get(events[0], None)
        if handler:
            handler.trigger(*events[1:])
            self.clean(events[0])

        return self

    # ------------------------------------------------------------------------ #
    # Decorators

    def listener(self, *args, **kargs):
        '''To be used as a listener decorator'''
        def decorator(listener):
            return self.registerListener(listener, *args, **kargs)
        return decorator

    def callback(self, *args, **kargs):
        '''To be used as a callback decorator (oneshot)'''
        def decorator(callback):
            return self.registerCallback(callback, *args, **kargs)
        return decorator

    def coroutine(self, *args, **kargs):
        '''To be used as a callback decorator (oneshot)'''
        def decorator(coroutine):
            return self.registerCoroutine(coroutine, *args, **kargs)
        return decorator

    # ------------------------------------------------------------------------ #
    # Awaitables

    @types.coroutine
    def registerNext(self, *next):
        '''Register the current coroutine to a new event'''
        def action(coroutine, *current):
            events = next if next else current
            self.registerCoroutine(coroutine, *events)
        return (yield action)
