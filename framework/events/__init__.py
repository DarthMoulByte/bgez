from .EventHandler import *
from .Handle import *

from bgez.framework.bases import Process
from bgez import Service
from bgez import utils

import types

services = (Service.GlobalEvents,)
for service in services:
    for member in dir(service):
        if not member.startswith('_'):
            globals().setdefault(member, getattr(service, member))

@types.coroutine
def whoami():
    '''Returns the current handle to the coroutine'''
    def action(coroutine, *args):
        return coroutine
    return (yield action)

@types.coroutine
def skip(frames=1, event=Process.Event.PRE_DRAW_SETUP, handler=None):
    event = Process.Event(event)
    if frames < 1: return
    if handler is None:
        handler = Service.GlobalEvents
    yield from handler.registerNext(
        event, frames + utils.getTick())
