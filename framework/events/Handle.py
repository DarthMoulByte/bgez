from collections import defaultdict
from collections import deque

__all__ = ['Handle']

class Handle(object):
    '''Stores references of registered objects'''
    __slots__ = '_handles'

    def __init__(self, *args, **kargs):
        self._handles = defaultdict(deque)
        super().__init__(*args, **kargs)

    def __contains__(self, object):
        return object in self._handles

    @property
    def empty(self):
        return not any(collection
            for collection in self._handles.values())

    def register(self, object, collection):
        self._handles[object].append(collection)
        return self

    def unregister(self, *objects):
        if not objects:
            objects = tuple(self._handles)

        for object in objects:
            collections = self._handles.pop(object)
            while collections:
                collection = collections.popleft()
                if object in collection:
                    collection.remove(object)

        return self
