from bgez.framework.threading import Thread
from bgez import config

__all__ = ['LivingThread']

class LivingThread(Thread):

    DEFAULT_TIMEOUT = 0.0001

    def __init__(self, timeout=DEFAULT_TIMEOUT, **kargs):
        super().__init__(**kargs)
        self.timeout = timeout

    def run(self):
        while not self.killFlag.is_set():
            if self.process(*self._args, **self._kwargs) is True:
                self.kill()
            self.killFlag.wait(self.timeout)
