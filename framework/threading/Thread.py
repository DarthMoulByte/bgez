from bgez.framework.bases import Tracked
from bgez import utils
from bgez import config

from weakref import WeakSet, finalize
import threading
import queue

__all__ = ['Thread']

class Thread(Tracked, threading.Thread):

    QUEUE = queue.Queue
    QUEUE_MAX_SIZE = 0

    @staticmethod
    def synchronized(method):
        def modified(self, *args, **kargs):
            with self.lock:
                return method(self, *args, **kargs)
        return modified

    def __init__(self, **kargs):
        super().__init__(**kargs)
        # The synchronous flag for kill request
        self.killFlag = threading.Event()
        # The thread-safe lock for safe operations (@Thread.synchronized)
        self.lock = threading.Lock()
        # The queue to pass things down
        self.queue = self.QUEUE(kargs.get('maxsize', self.QUEUE_MAX_SIZE))
        # List of callbacks to execute on ticks for this thread
        self.onTickCallbacks = []

    def finalize(self):
        """Override this method to define finalizing actions"""
        pass

    def kill(self, wait=0):
        self.killFlag.set()
        utils.ftry(self.finalize)
        self.join(wait)

    @property
    def killed(self):
        return self.killFlag.is_set()

    def sync(self):
        """Override this function for process on each logic tick"""
        pass

    def onTick(self, callback):
        self.onTickCallbacks.append(callback)
        return callback

    def tick(self):
        utils.ftry(self.sync)
        for callback in self.onTickCallbacks:
            utils.ftry(callback, self)

    def run(self):
        return self.process(*self._args, **self._kwargs)

    def process(self, *args, **kargs):
        """Override this function with your process in the thread"""
        if self._target is None:
            raise AttributeError('no target function')
        return self._target(*args, **kargs)

    def __bool__(self):
        """Syntaxic sugar"""
        return self.killed
