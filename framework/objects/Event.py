__all__ = ['Event']

class Event(dict):
    DEFAULT_VALUE = 0

    def __getattr__(self, attr):
        return self.get(attr, self.DEFAULT_VALUE)
