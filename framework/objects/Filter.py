from bgez.framework.bases import Process
from bgez import utils

__all__ = ['Filter']

class Filter(Process):

    UNIFORMS_PREFIX = 'bgez_'

    @classmethod
    def getUniformName(cls, name):
        return '{}{}'.format(cls.UNIFORMS_PREFIX, name)

    def __init__(self, shader_path, passindex=0):
        self.shader_path = utils.getResourcePath(shader_path)
        self.passindex = passindex
        self.uniforms = {}

    def getShaderSource(self):
        with open(self.shader_path, 'r') as f:
            return f.read()

    def setUniform(self, name, value):
        self.uniforms[self.getUniformName(name)] = value

    def getUniform(self, name):
        return self.uniforms[self.getUniformName(name)]

    def delUniform(self, name):
        del self.uniforms[name]

    def __setitem__(self, item, value):
        return self.setUniform(item, value)

    def __getitem__(self, item):
        return self.getUniform(item)

    def __delitem__(self, item):
        return self.delUniform(item)

    def __iter__(self):
        return iter(self.uniforms.items())
