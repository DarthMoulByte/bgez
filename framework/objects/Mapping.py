from bgez.framework.bases import Toggleable
from bgez import MetaComponent, Component
from bgez import utils

from collections import defaultdict
import typing as T

__all__ = ['MetaMapping', 'Mapping']

class MetaMapping(MetaComponent):
    """This class holds the class methods for the mapping object"""

    def __init__(cls, *args, **kargs): # Class initialization
        cls.ComponentMappings = defaultdict(cls)
        super().__init__(*args, **kargs)

    def __getattr__(cls, attr):
        return cls.bind(attr)

    def GetMapping(cls, id:str):
        """Returns the mapping object linked to the klass 'id'"""
        return cls.ComponentMappings[id]

    def AddMapping(cls, callback, *actions, id:str=None) -> None:
        id = utils.getQualName(callback, 1) \
            if id is None else id
        mapping = cls.GetMapping(id)
        mapping.add(callback, *actions)

    def bind(cls, *actions, id:str=None) ->  T.Callable:
        def decorator(callback):
            cls.AddMapping(callback, *actions, id=id)
            return callback
        return decorator

class Mapping(Component, metaclass=MetaMapping):
    """
    The idea for the end user is to have a set of Actions mapped to callbacks.
    But callbacks can be bound to multiple actions as well.
    The user should think about linking actions to functions.
    When behind the hood, functions are linked to set of actions.
    """

    def __init__(self, **kargs) -> None:
        super().__init__(**kargs)
        self.CallbackBindings = defaultdict(set)

    def getBindings(self, callback:T.Callable):
        """Returns the actions bound to the callback"""
        return self.CallbackBindings[callback]

    def getCallbacks(self, *actions, ignore=()):
        callbacks = set()
        filter = set(actions) - set(ignore)
        for callback, bindings in self.CallbackBindings.items():
            if bindings & filter:
                callbacks.add(callback)
        return callbacks

    def add(self, callback:T.Callable, *actions):
        bindings = self.getBindings(callback)
        for action in actions:
            bindings.add(action)
