from bgez.framework.bases import Process
from bgez import Service
from bgez import utils

class Stage(Process):
    """Define your stages and their dependencies"""

    @classmethod
    def initialize(cls):
        """The scene filters (by name)"""
        cls.Scenes = [Service.SceneManager.getScene().name]

        """The manager to use for the library loading and stuff"""
        cls.LibraryManager = Service.LibraryManager

        """The entity manager used to spawn entities"""
        cls.EntityManager = Service.EntityManager

    """A list of blend file to import for your stage to run"""
    DEPENDENCIES = [
        # './Assets/MyFile1.blend': 'Scene',
        # './Assets/MyFile2.blend': 'Mesh',
        # ...
    ]

    def __init__(self):
        super().__init__()
        self.LoadStatusSet = set()
        self.loaded = False
        self.progress = 0.

    def ready(self, scene):
        """Override this for actions when stage is loaded"""
        pass

    def pre_draw_setup(self, scene):
        if not self.loaded:
            size = len(self.LoadStatusSet)
            if size:
                self.progress = sum(status.progress for status in self.LoadStatusSet) / size
            if all(status.finished for status in self.LoadStatusSet):
                self.loaded = True
                self.ready(scene)

        super().pre_draw_setup(scene)

    def load(self, **kargs):
        self.progress = 0.0
        self.loaded = False
        self.LoadStatusSet.add(
            self.LibraryManager.libLoad(*self.DEPENDENCIES, unload=True))
        self.start_dispatch(Service.SceneManager.getScene())

    def unload(self, **kargs):
        self.finalize_dispatch()
