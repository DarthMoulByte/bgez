from bgez.framework.bases import Toggleable
from bgez.framework.objects import Mapping
from bgez.framework.types import Mapped
from bgez import Component
from bgez import Service
from bgez import utils

from collections import defaultdict
from weakref import WeakSet
from enum import IntEnum

import bgez

__all__ = ['Input']

class Input(Toggleable):

    # The Mapping object to use
    Mapping = Mapping

    # Shortcut
    Keys = bgez.events

    class State(IntEnum):
        JUST_ACTIVATED = bgez.logic.KX_INPUT_JUST_ACTIVATED
        JUST_RELEASED = bgez.logic.KX_INPUT_JUST_RELEASED
        ACTIVE = bgez.logic.KX_INPUT_ACTIVE
        NONE = bgez.logic.KX_INPUT_NONE

        INACTIVE = NONE
        JUST_INACTIVATED = JUST_RELEASED

    ACTIVE_STATES = (State.JUST_ACTIVATED, State.ACTIVE)
    INACTIVE_STATES = (State.JUST_INACTIVATED, State.INACTIVE)

    SHORTCUTS = {
        'ONCEON': State.JUST_ACTIVATED,
        'ONCEOFF': State.JUST_RELEASED,
        'ON': State.ACTIVE,
        'OFF': State.NONE,
    }

    def __init__(self, name, *args,
        manager='InputManager', lockedMouse=False, **kargs):

        # Action => Controls/Mappings
        self.mappings = defaultdict(set)

        # Components to use
        self.bindings = defaultdict(utils.List)

        self.name = str(name)
        self.manager = Service.Get(manager) if isinstance(manager, str) \
            else manager
        self.manager.RegisterInput(self)
        self.lockedMouse = bool(lockedMouse)

        super().__init__(*args, **kargs)

    def executeCallbacks(self, event):
        """Execute the registered mappings for each bound entity"""
        for id, instances in self.bindings.items():
            klass = Component.GetComponentById(id)
            if issubclass(klass, Mapped):
                for instance in instances:
                    # print(instance)
                    for callback in instance.getMappedCallbacks(*event.keys()):
                        utils.ftry(callback, event)

            mappings = self.Mapping.GetMapping(id)
            triggered = mappings.getCallbacks(*event.keys())
            for instance in instances.copy():
                for callback in triggered:
                    utils.ftry(callback.__get__(instance, klass), data)

    def bind(self, component):
        self.bindings[component.GetQualName()].append(component)
        return self

    def unbind(self, component):
        self.bindings[component.GetQualName()].remove(component)
        return self

    def __getattr__(self, action):
        if action in self.mappings:
            return self.registerCallback(action)
        raise AttributeError(action)

    def __add__(self, mapping):
        action = mapping.action
        self.mappings[action].add(mapping)
        return self

    def add(self, *mappings):
        for mapping in mappings:
            self + mapping
        return self
