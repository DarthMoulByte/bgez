import bgez

__all__ = ['InputMapping']

class InputMapping(object):
    def __init__(self, action:str) -> None:
        self.action = action # type: str
        self.truthTick = 0 # type: int
        self.inverted = False
        self.value = 0

    def evaluate(self) -> bool:
        return False

    def process(self) -> None:
        return

    def invert(self):
        self.inverted = not self.inverted
        return self

    def getValue(self):
        return self.value

    def getInvertedValue(self):
        return -self.getValue()

    def getResult(self):
        return self.getInvertedValue() if self.inverted \
            else self.getValue()

    def __bool__(self) -> bool:
        return self.truthTick == bgez.CurrentTick

    def __neg__(self):
        return self.invert()
