from bgez.framework.objects import Input
from bgez.framework.inputs import InputMapping

import bgez

__all__ = ['MouseButton']

class MouseButton(InputMapping):
    def __init__(self, key, *states):
        self.states = set(Input.SHORTCUTS.get(state, state) for state in states)
        self.key = key

    def getActiveState(self):
        events = bgez.logic.mouse.events
        state = events[self.key]
        if len(self.states) == 0:
            return state if state != Input.State.NONE else None
        return state if state in self.states else None

    def evaluate(self):
        self.value = self.getActiveState()
        return self.value is not None
