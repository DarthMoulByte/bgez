from bgez.framework.inputs import InputMapping

import bgez

__all__ = ['MouseAxis']

class MouseAxis(InputMapping):

    AXISES = {
        'x': 0,
        'y': 1,
    }

    def __init__(self, axis):
        self.axis = self.AXISES.get(axis, axis)

    def getScreen(self):
        return (bgez.render.getWindowWidth(), bgez.render.getWindowHeight())

    def process(self):
        dim = self.getScreen()
        mpos = list(bgez.logic.mouse.position)
        if any(1 < p < 0 for p in mpos):
            self.value = 0
        else:
            self.value = (dim[self.axis] // 2) / dim[self.axis] - mpos[self.axis]
