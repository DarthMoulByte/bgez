from bgez.framework.objects import Input
from bgez.framework.inputs import InputMapping

import bgez

__all__ = ['AxisMapping']

class AxisMapping(InputMapping):
    def __init__(self, action, *axises, threshold=0.):
        super().__init__(action)
        self.threshold = threshold
        self.axises = axises

    def process(self):
        for axis in self.axises:
            axis.process()

    def evaluate(self):
        values = [axis.getValue() for axis in self.axises]
        minimum, maximum = min(values), max(values)
        self.value = minimum if abs(minimum) > abs(maximum) else maximum
        return abs(self.value) >= self.threshold
