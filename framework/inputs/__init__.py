from .InputMapping import *

from .ButtonMapping import *
from .KeyboardButton import *
from .MouseButton import *

from .AxisMapping import *
from .MouseAxis import *
