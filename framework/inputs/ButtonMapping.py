from bgez.framework.objects import Input
from bgez.framework.inputs import InputMapping

import typing as T

import bgez

__all__ = ['ButtonMapping']

class ButtonMapping(InputMapping):
    """
    ACTIVE          + NONE              = ACTIVE
    ACTIVE          + ACTIVE            = ACTIVE
    JUST_ACTIVATED  + ACTIVE            = ACTIVE
    JUST_RELEASED   + ACTIVE            = ACTIVE
    JUST_ACTIVATED  + JUST_RELEASED     = ACTIVE
    JUST_ACTIVATED  + JUST_ACTIVATED    = JUST_ACTIVATED
    JUST_RELEASED   + JUST_RELEASED     = JUST_RELEASED
    JUST_ACTIVATED  + NONE              = JUST_ACTIVATED
    JUST_RELEASED   + NONE              = JUST_RELEASED
    NONE            + NONE              = NONE

    NONE            = 00
    ACTIVE          = 11
    JUST_RELEASED   = 01
    JUST_ACTIVATED  = 10

    11 | 00 = 11
    11 | 11 = 11
    10 | 11 = 11
    01 | 11 = 11
    10 | 01 = 11
    10 | 10 = 10
    01 | 01 = 01
    10 | 00 = 10
    01 | 00 = 01
    00 | 00 = 00

    This looks like madness. And it is.
    But it is the full combination list between the states.
    RIP in Pepperoni.
    """
    STATE2INT = {
        Input.State.NONE:           0b00,
        Input.State.JUST_ACTIVATED: 0b01,
        Input.State.JUST_RELEASED:  0b10,
        Input.State.ACTIVE:         0b11,
    } # type: T.Dict[int, int]
    INT2STATE = {
        0b00: Input.State.NONE,
        0b01: Input.State.JUST_ACTIVATED,
        0b10: Input.State.JUST_RELEASED,
        0b11: Input.State.ACTIVE,
    } # type: T.Dict[int, int]

    @classmethod
    def StateMix(cls,
        state1:(int, None),
        state2:(int, None)
            ) -> (int, None):
        if state1 is None:
            return state2
        elif state2 is None:
            return state1
        return cls.INT2STATE[cls.STATE2INT[state1] | cls.STATE2INT[state2]]

    def __init__(self, action:str, *buttons) -> None:
        super().__init__(action)
        self.buttons = buttons

    def process(self) -> None:
        for button in self.buttons:
            button.process()

    def evaluate(self) -> bool:
        return any(button.evaluate() for button in self.buttons)

    def getValue(self) -> int:
        self.value = None # type: (int, None)
        for button in self.buttons:
            self.value = self.StateMix(self.value, button.getValue())
        return self.value
