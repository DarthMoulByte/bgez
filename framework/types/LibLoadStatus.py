from bgez import Component
from bgez import utils

import typing
import bge

__all__ = ['LibLoadStatus']

class LibLoadStatus(Component, bge.types.KX_LibLoadStatus):

    def __init__(self, status: bge.types.KX_LibLoadStatus) -> None:
        #super().__init__()
        self.loadingCallbacks = []

    def loading(self, callback) -> typing.Callable:
        """Decorator"""
        self.loadingCallbacks.append(callback)
        return callback

    def executeCallbacks(self) -> None:
        if self.finished: return
        for callback in self.loadingCallbacks:
            utils.ftry(callback, self)

    def __str__(self) -> str:
        return self.libraryName
