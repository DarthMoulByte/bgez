from bgez.framework.types import Entity
from bge.types import KX_GameObject

__all__ = ['GameObject']

class GameObject(Entity, KX_GameObject):
    pass
