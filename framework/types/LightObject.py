from bgez.framework.types import Entity
from bge.types import KX_LightObject

__all__ = ['LightObject']

class LightObject(Entity, KX_LightObject):
    pass
