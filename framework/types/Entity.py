from bgez.framework.bases import Process
from bgez.framework.bases import Tracked
from bgez.framework.types import Mapped
from bgez import Service
from bgez import utils

from mathutils import Vector

__all__ = ['Entity']

class Entity(Mapped, Process, Tracked):
    """
    This class is some kind of interface working only on KX_GameObjects
    It adds a few extra features to GameObjects.
    """

    """The EntityManager in use"""
    EntityManager = Service.EntityManager

    """Boolean: register the entity to the entity manager"""
    REGISTER = False

    """If LINK is a callable, will be used to apply the class"""
    LINK = None

    @classmethod
    def initialize(cls):
        """Registering the entity to the EntityManager"""
        if callable(cls.LINK):
            cls.EntityManager.link(cls.LINK)(cls)
        elif isinstance(cls.LINK, bool):
            cls.EntityManager.linkToObjects(cls.__name__)(cls)
        elif isinstance(cls.LINK, str):
            cls.EntityManager.linkToObjects(cls.LINK)(cls)
        elif cls.LINK is not None:
            raise AttributeError('LINK class property should be either a function or a string or None !')

    def __init__(self, _=None, *args, **kargs):
        """Should always take two params:
        (self, _object_)"""
        self.relatives = utils.List()
        self.construct()
        self.EntityManager.autoLink(self, children=True, groupMembers=True)
        if self.REGISTER: self.registerEntity()
        super().__init__(*args, **kargs)

    def registerEntity(self):
        return self.EntityManager.register(self)

    def unregisterEntity(self):
        return self.EntityManager.unregister(self)

    def construct(self):
        """This is the pseudo __init__ for easier instance writing."""

    def destroy(self):
        """This is called when .endObject is triggered"""

    def hit(self, object, hit=None, normal=None, data=None):
        """Default hit function."""

    def endObject(self, children=False, groupMembers=False, relatives=False):
        '''Add recursion to the endObject function'''
        self.unregisterEntity()
        self.destroy()

        super().endObject()
        if children and self.children:
            for child in self.children:
                child.endObject(
                    children=children,
                    groupMembers=groupMembers,
                    relatives=relatives)
        if groupMembers and self.groupMembers:
            for member in self.groupMembers:
                member.endObject(
                    children=children,
                    groupMembers=groupMembers,
                    relatives=relatives)
        if relatives and getattr(self, 'relatives', None) is not None:
            for relative in self.relatives:
                relative.endObject(
                    children=children,
                    groupMembers=groupMembers,
                    relatives=relatives)

    def getChildren(self, *filters, recursive=True):
        """Finds children from some filters."""
        matches = []
        children = self.childrenRecursive if recursive else self.children
        for child in children:
            child = self.EntityManager.autoLink(child)[0]
            if all(filter(child) for filter in filters):
                matches.append(child)
        return matches

    def getChildrenByName(self, *child_names, recursive=True):
        return self.getChildren(lambda child: child.name in child_names, recursive=recursive)

    def getChildrenByProp(self, *property_names, recursive=True):
        return self.getChildren(lambda child: any(prop in property_names for prop in child.getPropertyNames()), recursive=recursive)

    def getParent(self):
        return self.EntityManager.autoLink(self.parent)[0]

    def getParents(self):
        parent = self.getParent()
        if parent is not None:
            papies = parent.getParents()
            papies.append(parent)
            return papies
        return []

    def getFamily(self, *filters):
        top = self.getParents()[0]
        return top.getChildren(*filters, recursive=True)

    def getDependents(self, children=True, groupMembers=True):
        """Retreive elements from both children and group members."""
        dependents = []
        if children and self.childrenRecursive is not None:
            dependents.extend(self.childrenRecursive)
        if groupMembers and self.groupMembers is not None:
            dependents.extend(self.groupMembers)
        for object in dependents:
            dependents.extend(self.__class__.getDependents(object, children=children, groupMembers=groupMembers))
        return dependents

    def getWorldPositionFromOffset(self, vector):
        """Gets the world position from a local offset."""
        return self.worldOrientation * Vector(vector) + self.worldPosition

    def rayCast(self, objto, objfrom=None, dist=0, prop='', face=0, xray=0, poly=0, mask=0b1111111111111111):
        """Provides a keyworded interface to the actual rayCast."""
        return super().rayCast(objto, objfrom, dist, prop, face, xray, poly, mask)

    def rayCastTo(self, target, dist=0, prop=''):
        """Provides a keyworded interface to the actual rayCastTo."""
        return super().rayCastTo(target, dist, prop)

    def localRayCast(self, ray, dist=None, default=None, **kargs):
        ray = Vector((ray))

        if dist is None: dist = ray.magnitude
        target = self.worldPosition + self.getAxisVect(ray)
        hitdata = list(self.rayCast(target, dist=dist, **kargs))
        if hitdata[0] is None:
            hitdata[1] = (target if default is None else default)
        return hitdata

    def hasLineOfSight(self, target, *args, **kargs):
        object = self.rayCastTo(target, *args, **kargs)
        return object is target
