from bgez import Component
from bgez import Service

import bge

__all__ = ['Scene']

class Scene(Component, bge.types.KX_Scene):
    '''Extended Scene class'''

    EntityManager = Service.EntityManager

    def addObject(self, name, reference=None, timeout=0, *args,
    klass=None, **kargs):
        object = super().addObject(name, reference, timeout, *args, **kargs)
        if klass is None:
            object = self.EntityManager.autoLink(object,
                children=True, groupMembers=True)[0]
        else: object = klass(object)
        return object

    def addAsset(self, asset, *args, **kargs):
        '''Adds an asset from the class'''
        if asset.Library().libraryName not in bge.logic.LibList():
            return print('<Name({})@Class({})> asset library is not loaded, cannot add it.'.format(asset.Name, asset.GetQualName()))
        return self.addObject(asset.Name, *args,
            klass=asset, **kargs)

    def clearObjects(self, filter=None, keep=None):
        '''Clears all objects, can be filtered

        param: filter: Callable returning True for deletion, False else
        param: keep: List of string or object to not delete'''

        if keep:
            filter = lambda object: (
                (object.name not in keep)
                and (object not in keep))

        for object in self.objects:
            if filter is None or filter(object):
                object.endObject()
