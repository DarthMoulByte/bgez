from bgez.framework.types import Entity
from bge.types import KX_Camera

__all__ = ['CameraObject']

class CameraObject(Entity, KX_Camera):

    def setCamera(self, copy=False):
        '''Sets the camera as the active camera'''
        old = self.scene.active_camera
        if copy:
            self.projection_matrix = old.projection_matrix
            self.lens = old.lens
            self.near = old.near
            self.far = old.far
        self.scene.active_camera = self
        return old

    def endObject(self, *args, **kargs):
        '''Makes sure to swap to a valid camera'''

        if self is self.scene.active_camera:
            default = self.scene.objects.get('bgez.camera.default', None)

            if default is None:
                for camera in self.scene.cameras:
                    if camera is not self:
                        default = camera
                        break

            if default is None:
                raise RuntimeError(
                    'You destroyed the last camera, BGE could crash.')

            self.scene.active_camera = default

        super().endObject(*args, **kargs)
