from bgez import Component

from collections import OrderedDict

__all__ = ['Mapped']

class Mapped(Component):
    MAPPING_INHERITANCE = 'copy'
    _MAPPINGS = {}

    @classmethod
    def classinit(cls, name, bases, namespace, **kargs):
        if cls.MAPPING_INHERITANCE == 'copy':
            cls._MAPPINGS = cls._MAPPINGS.copy()
        elif cls.MAPPING_INHERITANCE == 'clear':
            cls._MAPPINGS = {}
        for name, member in namespace.items():
            if hasattr(member, '__mapped__'):
                for action in member.__mapped__:
                    cls._MAPPINGS[action] = name
        super().classinit(**kargs)

    @classmethod
    def bind(cls, *actions):
        def decorator(callback):
            if not hasattr(callback, '__mapped__'):
                callback.__mapped__ = []
            for action in actions:
                callback.__mapped__.append(action)
            return callback
        return decorator

    def getMappedCallbacks(self, *actions):
        callbacks = OrderedDict()
        for action in actions:
            if action in self._MAPPINGS:
                member = self._MAPPINGS[action]
                callbacks[action] = getattr(self, member)
        return list(callbacks.values())
