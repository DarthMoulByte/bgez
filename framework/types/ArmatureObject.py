from bgez.framework.types import Entity
from bge.types import BL_ArmatureObject

__all__ = ['ArmatureObject']

class ArmatureObject(Entity, BL_ArmatureObject):
    pass
