from bgez.framework.libraries import AssetLibrary
from bgez.framework.types import Entity
from bgez import MetaComponent
from bgez import utils


__all__ = ['Asset', 'AssetException']

class AssetException(ImportError): pass

class Asset(Entity):
    """This entity-like object should be directly aware
     of the object it defines, in order to avoid lookups."""
    REFERENCE = None

    @classmethod
    def initialize(cls):
        if cls.REFERENCE and cls.LINK and not isinstance(cls.LINK, str):
            cls.LINK = cls.Name
        super().initialize()

    @utils.classproperty
    def Name(instance, klass):
        return klass.getObjectName()

    @classmethod
    def GetReference(cls, i=None):
        split = cls.REFERENCE.split(':', 2)
        if len(split) < 2:
            raise AssetException('The Asset reference must respect the format: "path/to/blend:objectName"')
        return split if i is None else split[i]

    @classmethod
    def getObjectName(cls):
        return cls.GetReference(1)

    @classmethod
    def Library(cls, **kargs):
        return AssetLibrary(cls.GetReference(0), **kargs)
