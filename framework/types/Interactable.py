from bgez.framework.objects import Mapping
from bgez import Component
from bgez import utils

__all__ = ['Interactable']

class Interactable(Component):

    Mapping = Mapping

    @utils.supermethod
    def interact(self, action, *args, **kargs):
        mapping = self.Mapping.GetMapping(utils.getQualName(self))
        for callback in mapping.getCallbacks(action):
            utils.ftry(callback, *args, **kargs)
