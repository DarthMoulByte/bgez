from bgez.framework.types import Entity
from bge.types import KX_NavMeshObject

__all__ = ['NavMeshObject']

class NavMeshObject(Entity, KX_NavMeshObject):
    pass
