from bgez.framework.bases import Toggleable
from bgez import Component
from bgez import utils

from weakref import ref
from enum import Enum
import bge

__all__ = ['Process']

class Process(Toggleable):

    class Event(Enum):
        START           = 'start'
        TICK            = 'tick'
        PRE_DRAW_SETUP  = 'pre_draw_setup'
        PRE_DRAW        = 'pre_draw'
        POST_DRAW       = 'post_draw'
        FINALIZE        = 'finalize'

    def __init__(self, *args, subProcessing=True, **kargs):
        """This pattern looks weird, but what it does is that is both
         initialize the class and the instances."""
        self.subProcessing = subProcessing

        self.processes = utils.List()
        super().__init__(*args, **kargs)

    def __iter__(self):
        return iter(self.processes)

    def __await__(self):
        return iter(())

    def register(self, *processes):
        """Register a process to run under own calls"""
        for process in processes:
            if process not in self.processes:
                self.processes.append(process)
                process.registering(self)

    def unregister(self, *processes):
        for process in processes:
            if process in self.processes:
                self.processes.remove(process)
                process.unregistering(self)

    def disableSubProcessing(self):
        self.subProcessing = False

    def enableSubProcessing(self):
        self.subProcessing = True

    def toggleSubProcessing(self):
        self.subProcessing = not self.subProcessing

    def registering(self, parent):
        """Override this method for actions on element registering"""

    def unregistering(self, parent):
        """Override this method for actions on element unregistering"""

    def start(self, *args, **kargs):
        """Override this method for actions on stage start"""

    def tick(self, *args, **kargs):
        """Override this method for actions on each stage tick"""

    def pre_draw_setup(self, *args, **kargs):
        """Override this method for actions on render frame setup"""

    def pre_draw(self, *args, **kargs):
        """Override this method for actions before render frame"""

    def post_draw(self, *args, **kargs):
        """Override this method for actions after render frame"""

    def finalize(self, *args, **kargs):
        """Override this method for actions on unloading"""

    def start_dispatch(self, *args, **kargs):
        if self.enabled:
            utils.ftry(self.start, *args, **kargs)
            if self.subProcessing:
                for process in self:
                    utils.ftry(process.start_dispatch, *args, **kargs)

    def tick_dispatch(self, *args, **kargs):
        if self.enabled:
            utils.ftry(self.tick, *args, **kargs)
            if self.subProcessing:
                for process in self:
                    utils.ftry(process.tick_dispatch, *args, **kargs)

    def pre_draw_setup_dispatch(self, *args, **kargs):
        if self.enabled:
            utils.ftry(self.pre_draw_setup, *args, **kargs)
            if self.subProcessing:
                for process in self:
                    utils.ftry(process.pre_draw_setup_dispatch, *args, **kargs)

    def pre_draw_dispatch(self, *args, **kargs):
        if self.enabled:
            utils.ftry(self.pre_draw, *args, **kargs)
            if self.subProcessing:
                for process in self:
                    utils.ftry(process.pre_draw_dispatch, *args, **kargs)

    def post_draw_dispatch(self, *args, **kargs):
        if self.enabled:
            utils.ftry(self.post_draw, *args, **kargs)
            if self.subProcessing:
                for process in self:
                    utils.ftry(process.post_draw_dispatch, *args, **kargs)

    def finalize_dispatch(self, *args, **kargs):
        """This takes priority over anything.
        At least give a chance to finalize."""
        utils.ftry(self.finalize, *args, **kargs)
        for process in self:
            utils.ftry(process.finalize_dispatch, *args, **kargs)
