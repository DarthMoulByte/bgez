from bgez import Component
from bgez import utils

from collections import defaultdict
from weakref import WeakSet

__all__ = ['Tracked']

class Tracked(Component):

    # QualifiedName(Class) => List(Class_Instances)
    References = defaultdict(utils.List)

    def _storeWeakReference(self) -> None:
        """Stores a references while it is still alive"""
        self.__class__.References[self.GetQualName()].append(self)

    @classmethod
    def Enumerate(cls):
        list = utils.List()
        for id, subList in cls.References.items():
            klass = cls.GetComponentById(id)
            if issubclass(klass, cls):
                list.extend(subList)
        return list

    @classmethod
    def GetInstances(cls) -> WeakSet:
        """Return the WeakSet refering to live instances"""
        return cls.References[cls.GetQualName()]

    def __init__(self, *args, **kargs) -> None:
        super().__init__(*args, **kargs)
        self._storeWeakReference()
