__all__ = ['Callback']

class Callback(dict):

    def __init__(self, callback, args, kargs, *a, **k):
        super().__init__(*a, **k)
        self.callback = callback
        self.args = args
        self.kargs = kargs

    def __call__(self):
        return self.callback(*self.args, **self.kargs)

    def __getattr__(self, attribute):
        return self[attribute]
