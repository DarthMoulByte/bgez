from bgez import Component
from bgez import utils

__all__ = ['Toggleable']

class Toggleable(Component):

    def __init__(self, *args, enabled:bool=True, **kargs) -> None:
        self.enabled = enabled
        super().__init__(*args, **kargs)

    def enable(self) -> None:
        self.enabled = True

    def disable(self) -> None:
        self.enabled = False

    def toggle(self) -> None:
        self.enabled = not self.enabled
