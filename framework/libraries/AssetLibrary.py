from bgez.framework.libraries import Library
from bgez import utils

__all__ = ['AssetLibrary']

class AssetLibrary(Library):

    def __init__(self, lib:str, *args, **kargs) -> None:
        if not lib.endswith('.blend'): lib += '.blend'
        super().__init__(utils.getResourcePath(lib), *args, **kargs)
