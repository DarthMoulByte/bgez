from bgez import Component
from bgez import config

import bge

__all__ = ['Library']

class Library(object):

    VERBOSE = False
    LOAD_ACTIONS = True
    LOAD_SCRIPTS = False

    LibLoadStatusFallback = config.get('Fallbacks')['LibLoadStatus']

    def __init__(self, libraryName: str, type: str = 'Scene',
    *args, **kargs) -> None:
        self.libraryName = libraryName
        self.type = type
        self.args = args
        self.kargs = kargs
        self.libstatus = None

        self.kargs.setdefault('load_actions', self.LOAD_ACTIONS)
        self.kargs.setdefault('load_scripts', self.LOAD_SCRIPTS)
        self.kargs.setdefault('verbose', self.VERBOSE)

    def load(self) -> bge.types.KX_LibLoadStatus:
        status = bge.logic.LibLoad(self.libraryName, self.type,
            *self.args, **self.kargs)
        self.libstatus = Component.GetComponentById(
            self.LibLoadStatusFallback)(status)
        return self.libstatus

    def unload(self) -> None:
        if self.libstatus is None:
            raise RuntimeError('{} was not loaded'.format(self.libraryName))
        bge.logic.LibFree(self.libstatus.libraryName)
        self.libstatus = None
