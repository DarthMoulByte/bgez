from bgez.framework.types import CameraObject
from bgez.framework.types import Mapped

from bgez.framework.objects import Input
from bgez.framework.inputs import (
    ButtonMapping, KeyboardButton, MouseButton,
    AxisMapping, MouseAxis)

import mathutils

__all__ = ['FreeCamera']

class FreeCamera(CameraObject):

    LINK = staticmethod(lambda object: object.name == '__default__cam__')

    SPEED = .1

    controls = Input('bgez.freecamera.controls', lockedMouse=True).add(
        ButtonMapping('forward',
            KeyboardButton(Input.Keys.ZKEY),
            KeyboardButton(Input.Keys.WKEY),),
        ButtonMapping('backward',
            KeyboardButton(Input.Keys.SKEY),),
        ButtonMapping('left',
            KeyboardButton(Input.Keys.QKEY),
            KeyboardButton(Input.Keys.AKEY),),
        ButtonMapping('right',
            KeyboardButton(Input.Keys.DKEY),),
        ButtonMapping('up',
            KeyboardButton(Input.Keys.SPACEKEY),),
        ButtonMapping('down',
            KeyboardButton(Input.Keys.LEFTCTRLKEY),
            KeyboardButton(Input.Keys.LEFTSHIFTKEY),),

        AxisMapping('view_x',
            MouseAxis('x'),),
        AxisMapping('view_y',
            MouseAxis('y'),),
    )

    def construct(self):
        self.controls.bind(self)

    @Mapped.bind('forward', 'backward', 'left', 'right', 'up', 'down')
    def movement(self, event):
        vlocal = mathutils.Vector((0, 0, 0))
        vglobal = mathutils.Vector((0, 0, 0))

        if event.forward:
            vlocal.z -= 1
        if event.backward:
            vlocal.z += 1
        if event.left:
            vlocal.x -= 1
        if event.right:
            vlocal.x += 1

        if event.up:
            vglobal.z += 1
        if event.down:
            vglobal.z -= 1

        vlocal.normalize()
        vglobal.normalize()
        self.applyMovement(vlocal * self.SPEED, True)
        self.applyMovement(vglobal * self.SPEED, False)

    @Mapped.bind('view_x', 'view_y')
    def mouselook(self, event):

        # global Z rotation
        rotation = self.worldOrientation.to_euler()
        rotation.z += event.view_x
        self.worldOrientation = rotation

        # local X rotation
        rotation = self.localOrientation.to_euler()
        rotation.x += event.view_y
        self.localOrientation = rotation
