__all__ = ['GenAsFunc']

class GenAsFunc(object):
    """Wraps a generator function as a pure function.
    Adds a `generator()` refering to the original
     generator.

    >>> @utils.GenAsFunc
    ... def myGenerator(*args):
    ...     for arg in args:
    ...         yield arg
    ...     return 'END'
    ...
    >>> print(myGenerator(1, 2, 3))
    END
    >>> for i in myGenerator.generator(1, 2, 3):
    ...     print(i)
    1
    2
    3
    >>> #example_end
    """
    def __init__(self, genfunc):
        self.__gen_init__ = genfunc
        self.yielded = []
    def __get__(self, *args):
        return self.__class__(self.__gen_init__.__get__(*args))
    def __call__(self, *args, **kargs):
        generator = self.__gen_init__(*args, **kargs)
        self.yielded = []
        while True: # Watch out for infinite sequences...
            try:
                self.yielded.append(generator.send(None))
            except StopIteration as stop:
                return stop.value
    def generator(self, *args, **kargs):
        return self.__gen_init__(*args, **kargs)
