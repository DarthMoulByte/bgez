from ..general import setAttr
from . import methodwrapper

__all__ = ['SelfAware', 'selfAware']

class SelfAware(methodwrapper):
    """This object allows functions and methods to
     be aware of themselves, passing themselves as the
     first argument."""
    def __init__(self, func, bound=None, **kargs):
        super().__init__(func)
        setAttr(**kargs)(self)
        self._bound = bound
    def __get__(self, instance, klass):
        return self.__class__(self.__wrapped__, instance or klass)
    def __call__(self, *args, **kargs):
        if self._bound is not None:
            return self.__wrapped__.__get__(self)(self._bound, *args, **kargs)
        return self.__wrapped__.__get__(self)(*args, **kargs)

def selfAware(**kargs):
    """@Decorator.
    Allow a function to know itself (passed as first argument).

    >>> @selfAware
    ... def myFunc(self, ...):
    ...     # Now access to `self`, refering to `myFunc`
    """
    def decorator(func):
        aware = SelfAware(func)
        setAttr(**kargs)(aware)
        return aware
    return decorator
