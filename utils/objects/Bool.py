__all__ = ['Bool']

class Bool(object):
    '''This object behaves a bit like a switch'''

    def __init__(self, state:bool=False):
        self.state = state

    def __nonzero__(self):
        return self.state

    def __bool__(self):
        return self.state

    def __repr__(self):
        return '<{}: {} at 0x{:X}>'.format(
            self.__class__.__name__, self.state, id(self))

    def set(self, state:bool):
        self.state = state
        return self

    def toggle(self):
        self.state = not self.state
        return self

    def enable(self):
        self.state = True
        return self

    def disable(self):
        self.state = False
        return self

    def __enter__(self):
        return self.toggle()

    def __exit__(self, *args):
        self.toggle()
