import bge

__all__ = ['List']

class List(list):
    '''This lists filters its own elements'''

    def setFilter(self, func):
        '''Can be used as a decorator'''
        self.filter = func

    def filter(self, item):
        '''Default filter'''
        return not (isinstance(item, bge.types.PyObjectPlus) and item.invalid)

    def __iter__(self):
        for item in self[:]:
            if self.filter(item): yield item
            else: self.remove(item)
