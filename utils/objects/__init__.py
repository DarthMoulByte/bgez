class classproperty(object):
    def __init__(self, getter):
        self.__getter__ = getter
        self.__setter__ = None
    def setter(self, setter):
        self.__setter__ = setter
        return setter
    def __get__(self, instance, klass):
        return self.__getter__(instance, klass)
    def __set__(self, instance, klass, value):
        if self.__setter__ is None:
            raise AttributeError("can't set the attribute")
        self.__setter__(instance, klass, value)

class methodwrapper(object):
    def __init__(self, func):
        self.__wrapped__ = func
        self.__doc__ = func.__doc__
        self.__name__ = '{}@{}'.format(func.__name__, self.__class__.__name__)
        self.__module__ = func.__module__
        self.__qualname__ = func.__qualname__
        self.__annotations__ = func.__annotations__
    def __call__(self, *args, **kargs):
        return self.__wrapped__(*args, **kargs)
    def __get__(self, *args):
        return self.__class__(self.__wrapped__.__get__(*args))

from .SelfAware import *
from .GenAsFunc import *
from .List import *
from .Bool import *
