import traceback
import threading
import importlib
import weakref
import sys
import os

from bgez import ROOT

__all__ = [
    'getResourcePath',
    'synchronized',
    'importMember',
    'keepQualName',
    'getQualName',
    'weakMethod',
    'printErr',
    'bitmask',
    'invert',
    'setAttr',
    'clamp',
    'ftry',
]

def getResourcePath(resource:str) -> str:
    """Returns the absolute path to the resource, from path relative to the `bgez` package parent folder.

    >>> getResourcePath('./bgez')
    '/pathToProjet.../ProjectName/bgez'
    """
    if resource.startswith('/'):
        return os.path.realpath(resource)
    return os.path.realpath(os.path.join(ROOT, resource))

def synchronized(lock:threading.Lock):
    """To be used on functions to be 'thread-safe':

    >>> @synchronized(threadingLockObject)
    >>> def myFunction(...):
    ...    # code
    """
    def decorator(func):
        def modified(*args, **kargs):
            with lock:
                return func(*args, **kargs)
        return modified
    return decorator

def importMember(dotPath):
    """Import something from a module."""
    package, member = dotPath.rsplit('.', 1)
    module = importlib.import_module(package)
    return getattr(module, member)

def keepQualName(module:str=None, qualname:str=None):
    """This is a decorator decorator. T_T
    Makes a decorator keep a good parsable qualified name."""
    def metaDecorator(decorator):
        def superDecorator(func, *args, **kargs):
            modified = decorator(func, *args, **kargs)
            modified.__module__ = func.__module__ \
                if module is None else module
            modified.__qualname__ = '{}@{}'.format(
                func.__qualname__,
                getQualName(modified).replace('.', ':')
            ) if qualname is None else qualname
            return modified
        return superDecorator
    return metaDecorator

def getQualName(object:object, level:int=0) -> str:
    """Returns a path that look unique to the object's class.
    Its tries to give something unique per class from the module
     and the qualified name."""
    parts = object.__module__.split('.') + object.__qualname__.split('.')
    return '.'.join(parts[:len(parts)-level])

def weakMethod(method, *args, **kargs):
    """Lazy boundmethod wrapper."""
    weak = weakref.WeakMethod(method)
    return lambda: weak()(*args, **kargs) if weak() is not None else None

def bitmask(*bits, size=16, inverted=False) -> int:
    """Returns the integer representation for the given bits as layers"""
    mask = sum(1 << bit - 1 for bit in bits)
    if inverted:
        mask ^= (1 << size) - 1
    return mask

def invert(integer: int, size: int=16):
    """Inverts the bitmask repr of an integer"""
    return ((1 << size) - 1) ^ integer

def printErr(*objects, end:str='\n') -> None:
    """Print to stderr with `end` as last printed string."""
    sys.stderr.write(' '.join(str(object) for object in objects))
    sys.stderr.write(end)
    sys.stderr.flush()

def setAttr(**kargs):
    """Adds attributes to a function"""
    def decorator(func):
        for attr, value in kargs.items():
            setattr(func, attr, value)
        return func
    return decorator

def clamp(val:float, min:float=None, max:float=None):
    """Clamp the value between limits.

    Arguments:
        val: The value to clamp.
        min: The lower limit to clamp to. (optional)
        max: The upper limit to clamp to. (optional)
    """
    if (min is not None) and (min > val):
        return min
    elif (max is not None) and (max < val):
        return max
    else:
        return val

def ftry(func, *args, ftry_silent:bool=False, **kargs) -> None:
    """Tries to execute the given function with args/kargs, but catches the exception and printing it to stdout (if not silent)."""
    try:
        return func(*args, **kargs)
    except:
        traceback.print_exc()
        return Exception
