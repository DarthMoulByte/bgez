import mathutils
import bge

from bgez.extras import PIW

__all__ = [
    'getScene',
    'getObject',
    'addObject',
    'setCamera',
    'getCamera',
    'drawVect',
    'quatRot',
    'lerp',
]

def getScene(scene=None) -> bge.types.KX_Scene:
    """Get a Scene by name, or current if not supplied

    - scene: The scene name to get.
        > Defaults to current KX_Scene
        > If you pass a KX_Scene, it will be returned as it is.
    """
    if not scene:
        return bge.logic.getCurrentScene()
    elif isinstance(scene, bge.types.KX_Scene):
        return scene
    else:
        for s in bge.logic.getSceneList():
            if s.name == scene:
                return s

def getObject(
    object_name:str,
    scene=None,
    all=False) -> bge.types.KX_GameObject:
    """Get an object by name [and Scene object/name: Optional].

    - name: The name of the object.
        Must be in an inactive layer !
    - scene: The scene in which to spawn the object.
        > Defaults to current.
    """
    scene = getScene(scene)
    return PIW(
        [object for object in scene.objects if object.name == object_name]
    ) if all else scene.objects.get(object_name, None)

def addObject(
    object_name:str,
    reference=None,
    time:int = 0,
    scene = None) -> bge.types.KX_GameObject:
    """Adds an object into the scene.

    - name:string: The name of the object
    -
    """
    scene = getScene(scene)
    return scene.addObject(object_name, reference, time)

def setCamera(
    camera:bge.types.KX_Camera,
    scene=None) -> bge.types.KX_Camera:
    """Sets the current active camera for scene.

    - camera: The camera to set to.
        The KX_GameObject or name.
    - scene: The scene to set the active camera.
        > Default to current.
    """
    scene = getScene(scene)
    scene.active_camera = camera
    return camera

def getCamera(scene=None) -> bge.types.KX_Camera:
    """Gets the current active camera for scene.

    - scene: The scene to get the active camera from.
        > Defaults to current.
    """
    scene = getScene(scene)
    return scene.active_camera

def drawVect(
    position:mathutils.Vector,
    vector:mathutils.Vector,
    color:mathutils.Vector = (1, 1, 1)
        ) -> None:
    """Draws a vector at a given point.

    - position: The coordinates of the origin to draw.
    - vector: The vector to draw.
    - color: The color of the line drawn.
        > Defaults to white (1, 1, 1).
    """
    bge.render.drawLine(position, position + vector, color)

def quatRot(
    quat:mathutils.Quaternion,
    axis:mathutils.Vector,
    angle:float,
    local:bool = False
        ) -> mathutils.Quaternion:
    """Rotate a particular quaternion around a given axis.

    - quat: The quaternion to rotate.
    - axis: The axis around which to rotate (global space by default).
    - angle: The angle to rotate around the given axis.
    - local: If the rotation is to be done on a local axis.
    """
    if local:
        axis = quat * mathutils.Vector(axis)
    quat.rotate(mathutils.Quaternion(axis, angle))
    return quat

def lerp(
    a:(float, mathutils.Vector),
    b:(float, mathutils.Vector),
    ratio:float
        ) -> (float, mathutils.Vector):
    """Returns the values interpolated by ratio:

    >>> lerp(a:float, b:float, ratio:float)
    float

    >>> lerp(a:vector, a:vector, ratio:float)
    vector
    """
    iia = isinstance(a, collections.Iterable)
    iib = isinstance(b, collections.Iterable)

    Va = mathutils.Vector(a if iia else (a, 0))
    Vb = mathutils.Vector(b if iib else (b, 0))

    i = Va.lerp(Vb, ratio).to_tuple()
    return i if iia or iib else i[0]
