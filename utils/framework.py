import weakref
import bge

from . import selfAware, keepQualName

__all__ = [
    'currentFrame', 'incrementTick', 'getTick',
    'seconds',
    'delay',
]

currentFrame = 0 # type: int

def incrementTick() -> None:
    """Increments the current frame count"""
    global currentFrame
    currentFrame += 1

def getTick() -> int:
    """Returns the current frame"""
    return currentFrame

def seconds(n:float) -> int:
    """Converts seconds to logic ticks."""
    return int(n * bge.logic.getLogicTicRate())

@selfAware(registry=weakref.WeakKeyDictionary())
def delay(local, skip:int):
    """Function call cooldown (based on ticks).

    Arguments:
        skip: ticks to skip while in cooldown."""

    @keepQualName()
    def decorator(func:T.Callable):

        @selfAware()
        def delayed(self, *args, **kargs):
            if self._bound:
                self = self._bound
            lastcall = local.registry.get(self, -skip)
            if CurrentTick >= lastcall + skip:
                local.registry[self] = CurrentTick
                return func(*args, **kargs)

        return delayed
    return decorator
