import typing as T
import configparser
import glob
import os

from bgez import ROOT

CONF_PATH = os.path.join(ROOT, 'bgez', 'configurations') # type: str
CONF = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())

def parseDir(conf_dir:str, file='*.ini') -> None:
    for ini in glob.glob(os.path.join(conf_dir, file), recursive=True):
        CONF.read(ini)
###END

parseDir(CONF_PATH)
parseDir(ROOT, 'configuration.ini')
parseDir(ROOT, 'Configuration.ini')

def get(sectionName:str) -> T.Mapping[str, str]:
    return CONF[sectionName]

# Parse of the "user" config
parseDir(os.path.join(ROOT, get('Game')['Configurations']))
