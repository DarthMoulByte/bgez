"""
This module keeps a track of registered components, and a way to find them.
It defines the Component class, self registering.
"""
from bgez import utils
from bgez.extras import PythonFix
from bgez import log

import typing as T

__all__ = ['MetaComponent', 'Component']

class MetaComponent(type):

    # Stores components as QualifiedName(Class) => Class
    ComponentTable = {} # type: T.Dict[str, type]

    @classmethod
    def RegisterComponent(metacls:type, component:type) -> type:
        metacls.ComponentTable[utils.getQualName(component)] = component
        return component

    @classmethod
    def GetComponentById(metacls:type, id:str) -> type:
        return metacls.ComponentTable[id]

    @classmethod
    def Initialize(metacls:type):
        for component in metacls.ComponentTable.values():
            component.initialize()

    def __init__(cls:type, *args, **kargs) -> None:
        super().__init__(*args, **kargs)
        if hasattr(cls, 'classinit'):
            cls.classinit = PythonFix.ClosureFix(cls, cls.classinit)
            cls.classinit(*args, **kargs)
        cls.RegisterComponent(cls)

    def initialize(cls:type):
        """The default initialization"""

    def classinit(cls:type, *args, **kargs) -> None:
        """The default classinit method."""

class Component(metaclass=MetaComponent):
    """This is the `bgez` base component.
    It registers the class for future fuzzy lookups,
     and allows class initialization."""

    def __init_subclass__(cls, **kargs):
        """For Python 3.6+"""

    @classmethod
    def GetQualName(cls) -> str:
        return utils.getQualName(cls)
