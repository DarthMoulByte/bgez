from . import MetaComponent
from bgez import log

import traceback

__all__ = ['Service', 'MetaService']

class MetaService(MetaComponent):
    def __getattr__(cls, service):
        return cls.Get(service)

    def Register(cls, name=None, *args, **kargs):
        def decorator(klass):
            nonlocal name
            if name is None:
                name = klass.__name__
            cls.Services[name] = klass(*args, **kargs)
            return klass
        return decorator

    def Get(cls, service):
        try:
            return cls.Services[service]
        except KeyError as e:
            log.e('"{}" service not found. Available services at this point:'.format(service), tag='HELP')
            for name, service in cls.Services.items():
                log.e(' - {}: {}'.format(name, service))
            raise e

class Service(metaclass=MetaService):
    Services = {}
