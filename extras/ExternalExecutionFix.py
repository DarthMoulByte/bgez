from bgez.extras import Dummy

import sys

class FakeMember(Dummy):
    def __init__(self, owner, name, *args, **kargs):
        super().__init__(self, *args, **kargs)
        self.__name__ = '{}.{}'.format(owner.__name__, name)
        self.__module__ = owner.__module__
    def __default__(self, attr):
        return self.__class__(self.__name__, attr)
    def __str__(self):
        return self.__name__

class FakeModule(Dummy):
    def __init__(self, name, parent=None, *args, **kargs):
        super().__init__(*args, **kargs)
        if parent is not None:
            name = '{}.{}'.format(parent.__name__, name)
        self.__module__ = 'fake_{}'.format(name)
        self.__path__ = self.__module__
        self.__name__ = name
        self.__all__ = []
        sys.modules.setdefault(name, self)
    def addSubModule(self, name, **kargs):
        setattr(self, name, self.__class__(name, parent=self, **kargs))
        self.__all__.append(name)
    def __default__(self, attr):
        return FakeMember(self, attr)

# Faking everything from there:
#https://docs.blender.org/api/blender_python_api_current/contents.html#game-engine-modules

# Application modules
bpy = FakeModule('bpy')
bpy.addSubModule('context')
bpy.addSubModule('data')
bpy.addSubModule('ops')
bpy.addSubModule('types')
bpy.addSubModule('utils')
bpy.utils.addSubModule('previews')
bpy.addSubModule('path')
bpy.addSubModule('app')
bpy.app.addSubModule('handlers')
bpy.app.addSubModule('translations')
bpy.addSubModule('props')

# Standalone modules
mathutils = FakeModule('mathutils')
mathutils.addSubModule('geometry')
mathutils.addSubModule('bvhtree')
mathutils.addSubModule('kdtree')
mathutils.addSubModule('interpolate')
mathutils.addSubModule('noise')
freestyle = FakeModule('freestyle')
bgl = FakeModule('bgl')
blf = FakeModule('blf')
gpu = FakeModule('gpu')
gpu.addSubModule('offscreen')
aud = FakeModule('aud')
bpy_extras = FakeModule('bpy_extras')
idprop = FakeModule('idprop')
idprop.addSubModule('types')
bmesh = FakeModule('bmesh')

# Game engine modules
bge = FakeModule('bge')
bge.addSubModule('types')
bge.addSubModule('logic',
    getCurrentController = lambda: None,)
bge.addSubModule('render')
bge.addSubModule('texture')
bge.addSubModule('events')
bge.addSubModule('constraints')
bge.addSubModule('app')
