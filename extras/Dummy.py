__all__ = ['Dummy']

class Dummy(object):
    """This object is some kind of super-dummy object.
    It does everything, and nothing at the same time."""

    def __init__(self, *args, **kargs):
        self.__dict__.update(kargs)

    def __default__(self, attr):
        return self.__class__()

    def __getattr__(self, attr):
        return self.__dict__.setdefault(attr, self.__default__(attr))

    def __setattr__(self, attr, value):
        self.__dict__[attr] = value

    def __delattr__(self, attr):
        self.__dict__.pop(attr, None)

    def __getitem__(self, key):
        return self.__getattr__(key)

    def __setitem__(self, key, value):
        return self.__setattr__(key, value)

    def __delitem__(self, key):
        return self.__delattr__(key)

    def __call__(self, *args, **kargs):
        return self

    def __contains__(self, item):
        return False

    def __iter__(self):
        return iter(())

    def __index__(self):
        return 0

    def __int__(self):
        return 0

    def __float__(self):
        return 0.

    def __complex__(self):
        return 0j

    def __bytes__(self):
        return bytes()

    def __bool__(self):
        return False

    def __enter__(self):
        return self

    def __exit__(self, *args, **kargs):
        return False

    def __str__(self):
        return '<{}: {} at 0x{:x}>'.format(self.__class__.__name__, str(self.__dict__), id(self))

    def __repr__(self):
        return str(self)

    def __add__(self, item):
        return item

    def __sub__(self, item):
        return item

    def __mul__(self, item):
        return item

    def __div__(self, item):
        return item

    def __mod__(self, item):
        return item
