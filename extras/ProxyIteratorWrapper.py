import traceback

__all__ = ['ProxyIteratorWrapper', 'PIW']

class ProxyIteratorWrapper(object):
    """This class wraps an iterable object in order
     to proxy calls to each of its elements.
    (Think as jquery with collections)"""

    def __init__(self, iterable, silent=False):
        self.__wrapped__ = iterable
        self._silent = silent
        self._isMapping = \
            hasattr('keys', iterable) and hasattr('__getitem__')

    def __call__(self, methodname, *args, **kargs):
        if self._isMapping:
            for key in self.__wrapped__.keys():
                try:
                    getattr(self.__wrapped__[key], methodname)(*args, *kargs)
                except:
                    if not self._silent:
                        traceback.print_exc()
        else:
            for item in self.__wrapped__:
                try:
                    getattr(item, methodname)(*args, *kargs)
                except:
                    if not self._silent:
                        traceback.print_exc()

    def __getattr__(self, attr):
        try: # Get the attribute from the wrapped object
            return getattr(self.__wrapped__, attr)
        except AttributeError: # Now, look for a method in the items
            return lambda *args, **kargs: self(attr, *args, **kargs)

    def __setattr__(self, attr, value):
        if hasattr(self.__wrapped__, attr):
           setattr(self.__wrapped__, attr, value)
        if self._isMapping: # Performance might be an issue...
            for key in self.__wrapped__.keys():
                try:
                    setattr(self.__wrapped__[key], attr, value)
                except:
                    if not self._silent:
                        traceback.print_exc()
        else:
            for item in self.__wrapped__:
                try:
                    setattr(item, attr, value)
                except:
                    if not self._silent:
                        traceback.print_exc()

    def __getitem__(self, keys):
        return self.__wrapped__.__getitem__(keys)

    def __setitem__(self, keys, item):
        return self.__wrapped__.__setitem__(keys, item)

    def __delitem__(self, keys):
        return self.__wrapped__.__delitem__(key)

    def __contains__(self, item):
        return item in self.__wrapped__

    def __hash__(self):
        return hash(self.__wrapped__)

    def __iter__(self):
        return iter(self.__wrapped__)

    def __str__(self):
        return '<ProxyIterableWrapper around {} at {}>'.format(self.__wrapped__, id(self))

    def __repr__(self):
        return str(self)

# Shortcut:
PIW = ProxyIteratorWrapper
