# BGEz / Blender Game Ez
## _Blender game-making made Easy !_

### #Introduction
This Python project is a framework working at elevating the Blender Game Engine API, providing more abstract concepts and advanced mechanisms for you in order to actually focus on designing the way your game should behave, instead of how to implement it. At least to some extend.

The second equally import goal is to redesign the way we write the logic, but in a readable and maintenable way. Although **simplier does not mean simple**, the API is designed to allow you to organize your project.

### #Features
This framework already has a lot of useful features, allowing you to efficiently describe the logic of your game:

- **Object subclassing and extra sugar**:

    When conceiving a game, we often think in terms of "objects" or "agents", having some kind of place in your grand scheme of things, each with its own behaviour and properties. **BGEz** aims at providing you implicit mechanics to write your object oriented concept in the BGE and provides auto linking of your Python class to the actual GameObjects. Super [sweet](./doc/general/entities.md) !

- **Python mappings**:

    Defining properties and methods for your game logic model is nice, but you might want to interact with your instances in some way, thru inputs primarely. **BGEz** provides just this mechanism, in a more abstract way than what the BGE's original API provides: You can annotate your methods, and then use a mapping set to trigger your annoted [mappings](./doc/general/mappings.md).

- **Stages with library management**:

    Scenes in the BGE allow you to have separates levels, but from the way its managed behind the hood, you cannot store a scene in an external `.blend` file, and you are stuck with merging every scene in your game file. **BGEz** provides an alternative concept: Stages. Write a stage, defines which `.blend` files are required, and write the logic to be executed as long as the stage is live. Everything is **automatically dynamically loaded / freed as needed**. [How cool is that](./doc/general/stages.md) ?

- **Asynchronous events**:

    Sometimes, you need a bit more frames to do something, like simple Python animations or running a `while True` loop. The BGE won't like it. But **BGEz** does like it. Using the coroutine concept, you can run code that will not necessarly freeze your game, and "spread" the execution of your function over several frames, so that you are in control of the amount of work to do on each frame ! [Coroutines for the win](./doc/general/events.md) !

- Many more features, here for your comfort !
