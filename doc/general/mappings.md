# BGEz mappings

## #Introduction
Well, writing classes with method and stuff is cool, but how do we trigger actions from there ? That's the role of mappings.

## #Basic
Lets just make some entity respond to the space bar:

```python
from bgez.framework.types import Asset, GameObject

from bgez.framework.objects import Input
from bgez.framework.inputs import ButtonMapping, KeyboardButton

controls = Input('my_controls_identifier')
controls.add(
    ButtonMapping('jump', KeyboardButton(Input.Keys.SPACEKEY))
)

class MyObject(Asset, GameObject):
    REFERENCE = './MyProject/objects/cube.blend:cube'

    def construct(self):
        '''For every instance, actually bind the controls.
        The binding is "off" by default.'''
        controls.bind(self)

    @Asset.bind('jump')
    def myFunction(self, event):
        print('YES!')
```

Now, your entity will always respond to the `jump` event, that we configured to depend on a keyboard button: `SPACEKEY`.

The API is rather simple:

```python
controls = Input('identifier').add(
    # you can add as much events as you want

    ButtonMapping('event_id_1', KeyboardButton(Input.Keys.AKEY)),
    ButtonMapping('event_id_2', KeyboardButton(Input.Keys.BKEY)),

    ButtonMapping('multiple_bindings_example',
        KeyboardButton(Input.Keys.CKEY),
        KeyboardButton(Input.Keys.DKEY),
        KeyboardButton(Input.Keys.EKEY),
    )
)

# You can add later also:
controls += ButtonMapping('other_event', ButtonMapping(Input.Keys.ZKEY))
```

Basically, the concept behind is that instead of doing `if keypressed(key) is True:...` we define `actions`, that then can be binded to anything. Be it a button or an axis, anything. Although, you need to specify which type of mapping it is (`ButtonMapping` or `AxisMapping`, or any other kind of special mapping).

Available mappings:

```python
ButtonMapping('button_event'

    # KeyboardButton(Input.Keys.AKEY, 'the states on which to trigger...'),
    KeyboardButton(Input.Keys.AKEY,
        Input.State.JUST_ACTIVATED, Input.State.JUST_RELEASED),

    # No state specified defaults to "anything but NONE"
    MouseButton(Input.Keys.LEFTMOUSE)
)

# Axis mapping examples
AxisMapping('axis_event', MouseAxis('x'))
AxisMapping('other_axis_event', MouseAxis('y'))
```

Then, when you map something to a function, a dictionnary of events is passed as only argument:

```python
from bgez.framework.types import Asset, GameObject

from bgez.framework.objects import Input
from bgez.framework.inputs import AxisMapping, MouseAxis

controls = Input('mouselook')
controls.add(
    AxisMapping('axis_x', MouseAxis('x')),
    AxisMapping('axis_y', MouseAxis('y')),
)

class MyObject(Asset, GameObject):
    REFERENCE = './MyProject/objects/cube.blend:cube'

    def construct(self):
        '''For every instance, actually bind the controls.
        The binding is "off" by default.'''
        controls.bind(self)

    @Asset.bind('axis_x', 'axis_y')
    def myFunction(self, event):
        self.applyRotation((event.axis_x, event.axis_y, 0))
```

Easy, right ?
