# BGEz events

## #Introduction
Sometimes, you need to wait for a particular event to happen, so I implemented just this: register some callbacks to an event, and trigger them when you want.

## #Callbacks
Lets say you want to do something on a firing event:

```python
from bgez.framework import events

@events.on('fire')
def fireCallback(arg1, arg2):
    print('FIRED !', arg1, arg2)

# same as
def fireCallback(arg1, arg2):
    print('FIRED !', arg1, arg2)
events.registerCallback(fireCallback, 'fire')
```

Now, all you have to do, is to trigger the event from anywhere:

```python
def randomFunction():
    # do things
    events.trigger('fire', 'first param to pass to callbacks', 'second param')
```

Behind the scenes, the `events` modules works like an `events.EventHandler` object, but you could instanciate your own `EventHandler` object in order to have a separate and independent event triggering system from the "global one" that is the `events` module.

For instance:

```python
from bgez.framework import events

A = events.EventHandler()
B = events.EventHandler()

@A.on('test')
def test():
    print('A')

@B.on('test')
def test():
    print('B')

A.trigger('test')
# prints: "A"
# but B didn't fire !
```

The best feature about this event system, is the coroutine capability !
Thanks to this system, you can define functions that will run across several frames. For instance:

```python
from bgez.framework import events

@events.on('test')
async def myCoroutine():
    print('STARTED')

    i = 0
    while True:
        await events.skip(frames=1) # wait 1 frame
        print('HEY', i)
        i += 1

events.trigger('test')
```

This will run the callback `myCoroutine`, and execute the returned coroutine forever (thanks to the `while True` loop), without freezing your game.

The problem with the way it is written in the last exemple is that for each `test` event triggered, a new coroutine will be created from the function `myCoroutine`... In order to avoid this, and run only once a coroutine, you can write:

```python
from bgez.framework import events

async def myCoroutine():
    print('STARTED')

    i = 0
    while True:
        await events.skip(frames=1) # wait 1 frame
        print('HEY', i)
        i += 1

events.registerCoroutine(myCoroutine())
```

Here you go.
