# BGEz stages

## #Introduction
In the BGE, we could use scenes in order to arrange levels, and swap between them by loading different scenes. Although there are multiple problems with this approach, as scenes cannot easely be put in an external file (linking maybe, even then loading the main game file would autoload every dependent library, not really optimal). Instead, lets use "softer scenes" concepts: stages.

## #Basic
**BGEz** doesn't impose you to use stages, but it would be foolish to not use them in my opinion ! First, lets define a stage that will be the first thing loaded by the framework at game launch:

```python
from bgez.framework.objects import Stage
from bgez import Service

# This is an object used by the framework to manage external libs
from bgez.framework.libraries import AssetLibrary

from MyProject.my_classes.MyObject import MyObject

@Service.StageManager.MainStage
class MyMainStage(Stage):

    DEPENDENCIES = [
        AssetLibrary('./my_libs/heavy_file_test.blend', async=True),
        MyObject.Library(), # Asset objects can output the ref to their lib
    ]

    def start(self, scene):
        print('Stage started in scene', scene)

    def ready(self, scene):
        print('Every lib are finally loaded !')

        # "scene" objects can now spawn an entity from the Asset class !
        instance = scene.addAsset(MyObject)
        instance.worldPositon = 0, 0, -12 # x, y, z

        # More conventional call still possible
        another_object = scene.addObject('object_name')

    def post_draw(self, scene):
        print('SPAM', 'I am executed after each frame !')

    def finalize(self, scene):
        print('The game is closing, or I am being unloaded !')
```

Ok, there might be a bit of boiler plate code, but its really straight forward:
- `DEPENDENCIES` is a list of `Library` objects, they describe the way to manage external libraries. `AssetLibrary` takes a path relative to your project's root, you can specify if you want the loading to be asynchronous (not freeze the game) or not.
- `Asset` classes such as `MyObject` in this example can provide the external lib they require, through the classmethod `.Library(async=False)`.
- `MyMainStage.start(...)` is executed when your stage is created, right on.
- `MyMainStage.ready(...)` is executed when every library in `DEPENDENCIES` is loaded, because if you need an object from a lib, but the lib isn't loaded, then the BGE won't be able to find the object you would want.
- `MyMainStage.finalize(...)` is called when finalizing the stage, on unload or game exit.

Availables events executed each frame:
- `tick`
- `pre_draw_setup`
- `pre_draw`
- `post_draw`
