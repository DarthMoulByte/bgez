# BGEz entities

## #Introduction
When you think about your game, usually you think in abstract concepts, about the way your different elements are organized together. This is why **BGEz** allows you to define Python classes that would represent your concepts and link it to the actual GameObjects inside the Blender Game Engine.

## #Basic
The only thing you need to do is to create a file in your game logic project folder, no naming convention required, feel free to use your own, but make sure that the script is in the parsed game logic folder:

```python
from bgez.framework.types import Asset, GameObject

class MyClass(Asset, GameObject):
    REFERENCE = './path/to/blend/file.blend:object_name'

    def construct(self):
        '''Do your initialisation stuff here'''
        self.my_property = 1
```

And that would be around anything you would need.

Lets explain a bit what happens here:
- The `Asset` class we are importing and using allows you to later use the class in order to directly spawn an instance of your object, and also it allows you to specify in which `.blend` file is located the entity your are trying to describe, as well as its "Blender name", in order to be able to reference it later.
- The `GameObject` class is the actual type of your object. Make sure to use a type compatible with the original Blender type of the object you are trying to define: for instance, if you want to write a class for an armature object, use `ArmatureObject` class in place of the `GameObject` one. Its a BGE-related limitation.
- The `REFERENCE` class variable is something that the `Asset` class looks for in order to know which object we are describing. You need to write the relative path from the project's root to the asset's file, and also give its name. The format is `file_path:object_name`.

And that's basically it.

In its current state, this class only adds a property to the object.

But we could add many more attributes, look up for children objects (spawning an object also spawns its children), make some computations and move things around, etc...

```python
from bgez.framework.types import Asset, GameObject

from MyProject.my_classes.ChildA import ChildA
from MyProject.my_classes.ChildB import ChildB

class MyObject(Asset, GameObject):
    REFERENCE = './MyProject/objects/cube.blend:cube'

    def construct(self):
        self.childA = ChildA(self.children.get('childA'))
        self.childB = ChildB(self.children.get('childB'))

        # Doing silly things
        self.childA.removeParent()
        self.silly()

    def silly(self):
        self.childA.worldPosition = self.worldPositon
        self.childA.worldPosition.z += 5
```

This example show how you could use multiple class definition and making them come together. Its rather abstract in this case, but you can look at [demos](https://gitlab.com/bgez/demos) in order to see more concrete stuff.

Maybe you want to [drive your entities too](./mappings.md).
