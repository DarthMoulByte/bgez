# #Framework Knowledge Requirements

## 1. Blender
Yea, somehow a framework for the **Blender Game Engine** requires you to know how to use Blender to some extend.

Just in case: [How to Blender](https://www.blender.org/support/tutorials/).

## 2. Blender Game Engine
Yup, some minimal knowledge around the BGE could help. It is not mandatory, as this framework aims to get rid of logic bricks and allow you to use a workflow a bit different, but the basic idea behind logic ticks and how to optimize assets for the BGE are a plus.

Just in case: [How to BGE](https://docs.blender.org/manual/en/dev/game_engine/index.html).

## 3. Python
Finally, this is a Python-heavy/only framework. Logic bricks will still work in your game, but the idea is to use them to a uber-bare minimum, and use a Python higher API to control the execution of your game.

**Disclaimer**: Even tho the idea is to have 'Python projects'-like, don't expect to run a main.py and play the game, you still need to start the game from the `blenderplayer`, which will in turn load your modules (Although the idea of starting the game from a script seems doable, maybe later).

Python is a rather simple language, but some mechanisms are not so easy to grasp, and BGEz uses some itself in addition to the object model put at your disposal. So here is a list of concepts to be aware of:

- Object Oriented Programmation (OOP):
    - Just the way to write object's classes and the inheritance process, as this framework is a system of multiple classes inheriting from each other's...
- Decorators:
    - `@decorator` syntax, BGEz uses them a lot, as it allows to change the behavior of decorated elements, in addition to the builtin way of writing '_static_' methods with `@classmethod` or `@staticmethod`.
- Generators / Coroutines:
    - I implemented some neet functions that allow you to spread the execution of a Python's generator through a pool of ticks. This allow for complex operation that you could be able to split into steps to not freeze your game on 1 tick, but rather spread it over multiple frames. In a nutshell, generators are function that `yield`s values instead of `return`ing them, pausing the execution so that you can resume it from the yielded statement.

Again, just in case: [How to Python](https://www.python.org/about/gettingstarted/).

## 4. Python's Decorators
It is not a well known feature for beginners, but Python's decorators syntax is very useful to know. Basically, decorators are Python callables that takes as first argument a function or a class, and returns what the decorated object should be instead.

```python
def myDecorator(function):
    print(function)
    return function

# Decorator syntax:
@myDecorator
def randomFunc(a, b, c):
    return (a, b + c)

# The above syntax is identical to the following:
# (Note the variable holding the function being overriden)
def randomFunc(a, b, c):
    return (a, b + c)
randomFunc = myDecorator(randomFunc) # Override
```

This bit of code would print something like `<function randomFunc at 0x...>` because the decorator function `myDecorator` is called after `randomFunc` is created (not when called, it is called once: at creation time).

Now, even if you don't fully get it, just know when to use them: They decorate a function to do something funny with it, and that's all.

## 5. Python's Generators
Generators are a cool way to code in Python 3.5.+ !

```python
def myDecorator(arg1, arg2):
    for i in range(arg1):
        print('from generator:', i)
        for j in range(arg2):
            yield (i, j)

# myDecorator is currently a function.
# You can see it as a constructor to the actual generator.
# Lets initialize the generator:
gen = myGenerator(2, 4)

# Now we can cycle each yield with a for loop, for example:
for yielded in gen:
    print('from for:', yielded)

# The generator is now 'empty', it reached its end, and we would need
#  to initialize a new one to recycle through everything !
```

## 6. Python's coroutine concepts
Python introduced a new syntax to define coroutines in its 3.5+ versions, its super useful when thinking about asynchronous execution !

Super useful explanation: https://mdk.fr/blog/python-coroutines-with-async-and-await.html
