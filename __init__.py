import sys
import os

ROOT = os.path.dirname(os.path.dirname(__file__)) # type: str

from bgez.extras.ExternalExecutionFix import FakeModule
from .objects import *

import bge
import types as PyTypes
for key, item in bge.__dict__.items():
    if isinstance(item, (PyTypes.ModuleType, FakeModule)):
        sys.modules.setdefault('bgez.' + key, item)
        globals()[key] = item
del key, item

CONTROLLER = logic.getCurrentController()

from bgez.utils import *

import bgez.framework # Force load
init = lambda *args: None # Empty entry point

# Extended start message
from bgez import log
log.e('PROJECT ROOT: {}'.format(ROOT))
log.e(' Python:', sys.version)
log.e('Blender:', app.version_string)
log.e()

# Clear modules
del PyTypes
del bge
del sys
del os
